%
%	This script configure the simulation with the following parameter
%		district	:	start the simulation on which district a,b,c or d
%		sim_offset 	:	start the simulation at which time
%		sim_length	:	time to run the simulation
%		sim_step	: 	step of the simulation in sec.
%		sim_time	:	real time to wait between two step in sec.
%

function [district, sim_offset, sim_length, sim_step, sim_time] = simulation_initialize()
	global district
	global sim_offset
	global sim_length
	global sim_step
	global sim_time
	
	district = 'd';
	
	sim_offset = 28800;
	sim_length = 65400-28800; %24*60*60;
	sim_step = 10*60;
	sim_time = 10;
end