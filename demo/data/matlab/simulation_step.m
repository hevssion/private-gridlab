%
%	This script calcul the next values of Q (reactive power) based on the next P (active power) of the grid and the current
%	values of U, I, UL, PL, QL and SL for the different phase for the three houses of the district.
%
%	The system to regulate is three different PV profils for the three houses.
% 	The csv file is the pv.csv where the powers are taken from.
%	Each step in this file represent 10min IRL.
%
%	step is the current time of the simulation
%	ppv is a cell array containing the next active power of the three houses
%		first_power = ppv{1,1},
% 		second_power = ppv{1,2}
% 		third_power = ppv{1,3}
%
%	The table below help you to get the correct value from the readValue cell given in paramter in the following simulation_step function
%
% 		first house				second house			third house			feeder
% 		readvalue{1}{1} 		readvalue{2}{1} 		readvalue{3}{1}		readvalue{4}{1}		%U1
%  		readvalue{1}{2} 																		%U2
%  		readvalue{1}{3} 																		%U3
%  		readvalue{1}{4} 																		%UN
%  		readvalue{1}{5} 																		%I1
%  		readvalue{1}{6} 																		%I2
%  		readvalue{1}{7} 																		%I3
%  		readvalue{1}{8}  																		%IN
%  		readvalue{1}{9}  																		%UL12
%  		readvalue{1}{10}  																		%UL23
%  		readvalue{1}{11}  																		%UL31
%  		readvalue{1}{12}  																		%USUM
%  		readvalue{1}{13}  																		%ISUM
%  		readvalue{1}{14}  																		%PL1
%  		readvalue{1}{15}  																		%PL2
%  		readvalue{1}{16}  																		%PL3
%  		readvalue{1}{17}  																		%PA
%  		readvalue{1}{18}  																		%QL1
%  		readvalue{1}{19}  																		%QL2
%  		readvalue{1}{20}  																		%QL3
%  		readvalue{1}{21}  																		%QA
%  		readvalue{1}{22}  																		%SL1
%  		readvalue{1}{23}  																		%SL2
%  		readvalue{1}{24}  																		%SL3
%  		readvalue{1}{25}  																		%SA


function [q1, q2, q3] = simulation_step(step,ppv,readvalue)
global P_inv_A P_inv_B P_inv_C      %la puissance solaire est mesuree
global P_charge_A P_charge_B P_charge_C P_charge_D %la puissance consommateur est mesuree par un smartmeter
global phi_charge_A phi_charge_B phi_charge_C phi_charge_D

choix_algo=0;
%disp(['Appel de fonction temps: ' num2str(step/60/60) ' h']);

if choix_algo==2
    q1 = 0;
    q2 = 0;
    q3 = 0;
    
elseif choix_algo==1
    q1 = ppv{1,1};
    q2 = ppv{1,2};
    q3 = ppv{1,3};
    
else
    
    
    
    %%%%%%
    % 1) mise en forme pour traduire du format Python-Yann � Matlab-Moix:
    %     %Mesure du reseau:
    % P_inv_A=6000*3;
    % P_inv_B=5000*3;
    % P_inv_C=7000*3;      %la puissance solaire est mesuree
    % P_charge_A=00;
    % P_charge_B=00;
    % P_charge_C=00;
    % P_charge_D=0000; %la puissance consommateur est mesuree par un smartmeter
    % phi_charge_A=-0.96;
    % phi_charge_B=-0.96;
    % phi_charge_C=-0.96;
    % phi_charge_D=-0.96;
    
    %en utilisant la lecture des SICAM (en r�action) en considerant qu'il
    %n'y a que du PV:
    P_inv_A=-readvalue{1};
    P_inv_B=-readvalue{2};
    P_inv_C=-readvalue{3};     %la puissance solaire est mesuree et transmise par 
    P_charge_A=0;       %ATTENTION, pas encore fournit par Python de mani�re d�sagr�g�e!
    P_charge_B=0;       %ATTENTION, pas encore fournit par Python de mani�re d�sagr�g�e!
    P_charge_C=0;       %ATTENTION, pas encore fournit par Python de mani�re d�sagr�g�e!
    P_charge_D=0;      %la puissance consommateur est mesuree par un smartmeter %ATTENTION, pas encore fournit par Python de mani�re d�sagr�g�e!
    phi_charge_A=-acos(0.96);
    phi_charge_B=-acos(0.96);
    phi_charge_C=-acos(0.96);
    phi_charge_D=-acos(0.96);
    
    %en utilisant les futurs consignes comme si on pouvais anticiper la
    %production au prochain step:
     % P_inv_A=ppv{1,1};
    % P_inv_B=ppv{1,1};
    % P_inv_C=ppv{1,1}; 
    %PA
    %%%%%%
    % 2) lance l'optimisation
    
    solution_optim_district;
    
    
    %%%%%%%%%%
    %3) traduction des r�sultats Matlab-Moix � Python-Yann:
    %Q=S*sin(phi)
    %P=S*cos(phi)
    %Q=P/cos(phi)*sin(phi)=P*tan(phi)  phi!=0
    
    q1=tan(table_pilotage_opt(1))*P_inv_A;
    q2=tan(table_pilotage_opt(2))*P_inv_B;
    q3=tan(table_pilotage_opt(3))*P_inv_C;
    
    
    %disp(['Appel de fonction temps: ' num2str(step/60/60) ' h']);
    %disp(['P_PV1: ' num2str(P_inv_A) ' W, P_PV2: ' num2str(P_inv_B) ' W, P_PV3: ' num2str(P_inv_C) ' W']);
    % disp(['Q_PV1: ' num2str(q1) ' VAr, Q_PV2: ' num2str(q2) ' VAr, Q_PV3: ' num2str(q3) ' VAr']);
end
end

% 	readvalue{1}{21}
% 	readvalue{2}{21}
% 	readvalue{3}{21}
%
% 	persistent energyp
% 	if isempty(energyp)
% 		energyp = 0;
% 	end
%
% 	energyp = energyp + ppv{1,1} + ppv{1,2} + ppv{1,3};
