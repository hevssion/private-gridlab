from gridsim.cyberphysical.element import Actor
from gridlab.gridlabcyberphysicalsystem.district import bcolors

# ACTOR

class SimpleActor(Actor):
    def __init__(self, read_param, write_param):
        super(SimpleActor, self).__init__()

        self.read_params = read_param
        self.write_params = write_param

    def get_value(self, write_param):
        print bcolors.HEADER + '[SimpleActor] get_value' + bcolors.ENDC, write_param
        if write_param[0] == ParamType._P:
            return 1000
        elif write_param[0] == ParamType._Q:
            return -1000
        return 0

    def notify_read_param(self, read_param, data):
        print bcolors.OKBLUE + '[SimpleActor] notify_read_param' + bcolors.ENDC, read_param, data

    def get_debug_value(self, write_param):
        print '[SimpleActor] get_debug_value', write_param


from gridlab.gridlabcyberphysicalsystem.feeder import Feeder
from gridlab.paramtype import ParamType
from gridplot.plot.matplot import MatPlot
from gridsim.execution import RealTimeExecutionManager
from gridsim.simulation import Simulator
from gridsim.unit import units

from gridlab.actor.guiactor import PQGuiController

# MAIN

district = 'd'

sim = Simulator(RealTimeExecutionManager(10 * units.seconds))

readparam = [(ParamType._PA,), (ParamType._QA,)]
writeparam = [(ParamType._P, district, 1)]

simpleactor = SimpleActor(readparam, writeparam)

matplot = MatPlot('simple_plot')
matplot.create_plot_data('Act. P. [W]', [(ParamType._PA, district, 1),
                                         (ParamType._P, district, 1)])
matplot.create_plot_data('Rea. P. [Var]', [(ParamType._QA, district, 1)])

guicontroller = PQGuiController(readparam, writeparam)

feeder = Feeder(district)
feeder.initialize_district()
feeder.register_actor(simpleactor)
#feeder.register_actor(guicontroller)
feeder.register_district(sim.cyberphysical)
feeder.register_plot(matplot)

sim.cyberphysical.add_module_listener(matplot)

matplot.start_ploting()
#guicontroller.start_controller()

sim.reset()
sim.run(10 * units.seconds, 1 * units.seconds)
