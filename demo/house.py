"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""
import argparse
import os

from gridlabcyberphysicalsystem.gridlabhouse import GridLabHouse
from regulation.converter import FirstThirdQuadrantConverter, SecondFourthQuadrantConverter

from gridlab.paramtype import ParamType, ConverterType
from gridsim.controller.element.thermostat import Thermostat
from gridsim.cyberphysical.actor.battery import Battery
from gridsim.cyberphysical.actor.domestichotwater import Boiler
from gridsim.cyberphysical.actor.pqfilereader import PQFileReader
from gridsim.cyberphysical.actor.spaceheating import ElectroThermalHeaterCooler
from gridsim.cyberphysical.aggregator import SumAggregator
from gridsim.cyberphysical.simulation import CyberPhysicalModule
from gridsim.cyberphysical.simulation import MinimalCyberPhysicalModule
from gridsim.execution import RealTimeExecutionManager
from gridsim.iodata.input import CSVReader
from gridsim.simulation import Simulator
from gridsim.thermal.core import ThermalProcess, ThermalCoupling
from gridsim.thermal.element import TimeSeriesThermalProcess
from gridsim.timeseries import SortedConstantStepTimeSeriesObject
from gridsim.timeseries import TimeSeriesObject
from gridsim.unit import units

parser = argparse.ArgumentParser(description='Simulate the content of a file over the Gridlab District')

parser.add_argument('-d', '--district', help='Specify the district letter', required=True)

parser.add_argument('-dt', '--deltatime', help='Time interval for the simulation', required=True)
parser.add_argument('-rt', '--runtime', help='Total run time', required=True)
parser.add_argument('-t', '--realtime', help='Real time for the simulation', required=True)

args = vars(parser.parse_args())
print os.getcwd()

Simulator.register_simulation_module(CyberPhysicalModule)
Simulator.register_simulation_module(MinimalCyberPhysicalModule)

sim = Simulator(RealTimeExecutionManager(int(args['realtime']) * units.seconds))

# SpaceHeating
# create room for space heating
celsius = units(20, units.degC)
room = sim.thermal.add(ThermalProcess.room('room',
                                           50 * units.meter * units.meter,
                                           2.5 * units.metre,
                                           units.convert(celsius, units.kelvin)))

outside = sim.thermal.add(
    TimeSeriesThermalProcess('outside', SortedConstantStepTimeSeriesObject(CSVReader('csv/heater.csv')),
                             lambda t: t * units.hour,
                             temperature_calculator=
                             lambda t: units.convert(units(t, units.degC),
                                                     units.kelvin)))
sim.thermal.add(ThermalCoupling('room to outside',
                                10.0 * units.thermal_conductivity,
                                room, outside))

# create heat pump
writeparam = [(ParamType._P, args['district'], 1)]
readparam = [(ParamType._I1,)]

electrothermalheatercooler = ElectroThermalHeaterCooler('heater',
                                                        1 * units.kilowatt, 1.0,
                                                        room, readparam, writeparam)
heater = sim.electrical.add_cps_listener(electrothermalheatercooler)

target = units(20, units.degC)
hysteresis = 1 * units.delta_degC

sim.controller.add_cps_listener(Thermostat('thermostat',
                                           units.convert(target, units.kelvin),
                                           hysteresis, room, heater, 'on'))

# Boiler
boiler = Boiler('boiler', 1.5 * units.meter, 1 * units.meter, 0.5 * units.meter,
                units.convert(units(40, units.degC), units.kelvin),
                1 * (units.watt / (units.kelvin * (units.meter * units.meter))),
                100000 * units.watt, units.convert(units(10, units.degC), units.kelvin),
                TimeSeriesObject(CSVReader('csv/boiler.csv')),
                readparam, writeparam, None)

heater = sim.electrical.add_cps_listener(boiler)

target = units(50, units.degC)
hysteresis = 1 * units.delta_degC

sim.controller.add_cps_listener(Thermostat('thermostat',
                                           units.convert(target, units.kelvin),
                                           hysteresis,
                                           boiler, heater, 'on'))

# PV
pv = PQFileReader('pv', '', None, readparam, writeparam)

# Battery
battery = Battery('battery1', 0 * units.watt_hour, 200 * units.watt_hour, 1000 * units.watt, readparam, writeparam)

# Uncontrollable
uncontrollable = PQFileReader('uncontrollable', '', None, readparam, writeparam)

# Initialize gridlabcyberphysicalsystem
converter = {ConverterType._first_third_quadrant_pq: FirstThirdQuadrantConverter(),
             ConverterType._second_fourth_quadrant_pq: SecondFourthQuadrantConverter()}

writegridlabunit1 = [(ParamType._P, SumAggregator(-10000, 10000)),
                     (ParamType._Q, SumAggregator(-10000, 10000))]

gridunit1 = GridLabHouse("gridlabcyberphysicalsystemunit1", converter)

gridunit1.initialize(args['district'], 1, write_params=writegridlabunit1)

# gridunit1.add(electrothermalheatercooler)
# gridunit1.add(boiler)
# gridunit1.add(pv)
gridunit1.add(battery)
# gridunit1.add(uncontrollable)

sim.cyberphysicalmodule.add_cps_listener(gridunit1)

sim.minimalcyberphysicalmodule.add_cps_listener(battery)

sim.cyberphysicalmodule.add_module_listener(electrothermalheatercooler)
sim.cyberphysicalmodule.add_module_listener(boiler)
sim.cyberphysicalmodule.add_module_listener(pv)
sim.cyberphysicalmodule.add_module_listener(battery)
sim.cyberphysicalmodule.add_module_listener(uncontrollable)

# Start simulation
sim.reset()
print('start simulation')
sim.run(int(args['runtime']) * units.seconds, int(args['deltatime']) * units.seconds)
print('end simulation')
