"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

import argparse
import os

from gridlab.gridlabcyberphysicalsystem.feeder import Feeder
from gridlab.paramtype import ParamType
from gridman.activity import ActivityManager
from gridplot.plot.matplot import MatPlot
from gridsim.cyberphysical.actor.pqfilereader import PQFileReader
from gridsim.cyberphysical.regulation import RegulationManager
from gridsim.cyberphysical.security import SecurityManager
from gridsim.cyberphysical.simulation import CyberPhysicalModule, MinimalCyberPhysicalModule
from gridsim.execution import RealTimeExecutionManager
from gridsim.simulation import Simulator
from gridsim.unit import units

# Example of the file reader using the cyber-physical module with the following starting params
# -i pqfile.csv -d b -t 30 -rt 25 -dt 1

parser = argparse.ArgumentParser(description='Simulate the content of a file over the Gridlab District')
parser.add_argument('-i', '--infile', help='Input file for reading from the values (P,Q)', required=True)
parser.add_argument('-o', '--outfile', help='Output file for the logging values, (current,voltage,...)', required=False)

parser.add_argument('-d', '--district', help='Specify the district letter', required=True)

parser.add_argument('-dt', '--deltatime', help='Time interval for the simulation', required=True)
parser.add_argument('-rt', '--runtime', help='Total run time', required=True)
parser.add_argument('-t', '--time', help='Real time for the simulation', required=True)

# get argument from script parameters
args = vars(parser.parse_args())
print os.getcwd()

# open output log file
if 'outfile' not in args.keys() or args['outfile'] is None:
    args['outfile'] = os.getcwd() + '\log.csv'

########################################################################################################################
## File Actor
########################################################################################################################

# create read params for the fileactor
readparam = [(ParamType._PA,),
             (ParamType._PL1,), (ParamType._PL2,), (ParamType._PL3,),
             (ParamType._QA,),
             (ParamType._QL1,), (ParamType._QL2,), (ParamType._QL3,),
             (ParamType._SA,),
             (ParamType._SL1,), (ParamType._SL2,), (ParamType._SL3,),
             (ParamType._USUM,),
             (ParamType._U1,), (ParamType._U2,), (ParamType._U3,),
             (ParamType._ISUM,),
             (ParamType._I1,), (ParamType._I2,), (ParamType._I3,),
             (ParamType._PF,),
             (ParamType._PF_L1,), (ParamType._PF_L2,), (ParamType._PF_L3,),
             (ParamType._WpEd,), (ParamType._WpEs,), (ParamType._WqEi,), (ParamType._WqEc,)]

# create write params for the fileactor
writeparam = []
for d in list(args['district']):
    for i in range(1, 4):
        writeparam.append((ParamType._P, d, i))
        writeparam.append((ParamType._Q, d, i))

# create opcode dict values to decode input file csv file column
opcode = {'a1p': (ParamType._P, 'a', 1), 'a1q': (ParamType._Q, 'a', 1),
          'a2p': (ParamType._P, 'a', 2), 'a2q': (ParamType._Q, 'a', 2),
          'a3p': (ParamType._P, 'a', 3), 'a3q': (ParamType._Q, 'a', 3),
          'b1p': (ParamType._P, 'b', 1), 'b1q': (ParamType._Q, 'b', 1),
          'b2p': (ParamType._P, 'b', 2), 'b2q': (ParamType._Q, 'b', 2),
          'b3p': (ParamType._P, 'b', 3), 'b3q': (ParamType._Q, 'b', 3),
          'c1p': (ParamType._P, 'c', 1), 'c1q': (ParamType._Q, 'c', 1),
          'c2p': (ParamType._P, 'c', 2), 'c2q': (ParamType._Q, 'c', 2),
          'c3p': (ParamType._P, 'c', 3), 'c3q': (ParamType._Q, 'c', 3),
          'd1p': (ParamType._P, 'd', 1), 'd1q': (ParamType._Q, 'd', 1),
          'd2p': (ParamType._P, 'd', 2), 'd2q': (ParamType._Q, 'd', 2),
          'd3p': (ParamType._P, 'd', 3), 'd3q': (ParamType._Q, 'd', 3)}

# create the fileactor : pqfilereader
pqfilereader = PQFileReader('pqfilereader', args['infile'],
                            args['outfile'],
                            readparam, writeparam)

# configure the file by using the opcode dict specified above
pqfilereader.initFile(opcode)

########################################################################################################################
## TEST guiactor
########################################################################################################################

# from gridlab.actor.guiactor import start_window
# import threading
#
# guicontroller = PQGuiController(readparam, writeparam)
# t = threading.Thread(target=start_window, args=(guicontroller,))
# t.start()

########################################################################################################################
## Plot
########################################################################################################################

matplot = MatPlot('Test Graph')
matplot2 = MatPlot('Test2 Graph')

matplot.create_plot_data('Act. P. [W]', [(ParamType._PA, 'a', 1),
                                         (ParamType._PA, 'a', 2),
                                         (ParamType._PA, 'a', 3)])
matplot.create_plot_data('Rea. P. [Var]', [(ParamType._QA, 'a', 1),
                                         (ParamType._QA, 'a', 2),
                                         (ParamType._QA, 'a', 3)])
matplot.create_plot_data('Act. P. [W]', [(ParamType._PA, 'b', 1),
                                         (ParamType._PA, 'b', 2),
                                         (ParamType._PA, 'b', 3)])
matplot.create_plot_data('Rea. P. [Var]', [(ParamType._QA, 'b', 1),
                                         (ParamType._QA, 'b', 2),
                                         (ParamType._QA, 'b', 3)])
matplot.create_plot_data('Act. P. [W]', [(ParamType._PA, 'c', 1),
                                         (ParamType._PA, 'c', 2),
                                         (ParamType._PA, 'c', 3)])
matplot.create_plot_data('Rea. P. [Var]', [(ParamType._QA, 'c', 1),
                                         (ParamType._QA, 'c', 2),
                                         (ParamType._QA, 'c', 3)])
matplot.create_plot_data('Act. P. [W]', [(ParamType._PA, 'd', 1),
                                         (ParamType._PA, 'd', 2),
                                         (ParamType._PA, 'd', 3)])
matplot.create_plot_data('Rea. P. [Var]', [(ParamType._QA, 'd', 1),
                                         (ParamType._QA, 'd', 2),
                                         (ParamType._QA, 'd', 3)])
# get data from file
# x, y = pqfilereader.get_all_values(('a1p',), 1, 0, 26)
# # plot early data from file in the first and second figure
# matplot.early_plot_data(0, x, y)
# x, y = pqfilereader.get_all_values(('a2p',), 1, 0, 26)
# matplot.early_plot_data(0, x, y)
# x, y = pqfilereader.get_all_values(('a3p',), 1, 0, 26)
# matplot.early_plot_data(0, x, y)
#
# x, y = pqfilereader.get_all_values(('a1q',), 1, 0, 26)
# matplot.early_plot_data(1, x, y)
# x, y = pqfilereader.get_all_values(('a2q',), 1, 0, 26)
# matplot.early_plot_data(1, x, y)
# x, y = pqfilereader.get_all_values(('a3q',), 1, 0, 26)
# matplot.early_plot_data(1, x, y)
#
# x, y = pqfilereader.get_all_values(('b1p',), 1, 0, 26)
# matplot.early_plot_data(2, x, y)
# x, y = pqfilereader.get_all_values(('b2p',), 1, 0, 26)
# matplot.early_plot_data(2, x, y)
# x, y = pqfilereader.get_all_values(('b3p',), 1, 0, 26)
# matplot.early_plot_data(2, x, y)
#
# x, y = pqfilereader.get_all_values(('b1q',), 1, 0, 26)
# matplot.early_plot_data(3, x, y)
# x, y = pqfilereader.get_all_values(('b2q',), 1, 0, 26)
# matplot.early_plot_data(3, x, y)
# x, y = pqfilereader.get_all_values(('b3q',), 1, 0, 26)
# matplot.early_plot_data(3, x, y)
#
# x, y = pqfilereader.get_all_values(('c1p',), 1, 0, 26)
# matplot.early_plot_data(4, x, y)
# x, y = pqfilereader.get_all_values(('c2p',), 1, 0, 26)
# matplot.early_plot_data(4, x, y)
# x, y = pqfilereader.get_all_values(('c3p',), 1, 0, 26)
# matplot.early_plot_data(4, x, y)
#
# x, y = pqfilereader.get_all_values(('c1q',), 1, 0, 26)
# matplot.early_plot_data(5, x, y)
# x, y = pqfilereader.get_all_values(('c2q',), 1, 0, 26)
# matplot.early_plot_data(5, x, y)
# x, y = pqfilereader.get_all_values(('c3q',), 1, 0, 26)
# matplot.early_plot_data(5, x, y)

x, y = pqfilereader.get_all_values(('d1p',), 1, 0, 26)
matplot.early_plot_data(6, x, y)
x, y = pqfilereader.get_all_values(('d2p',), 1, 0, 26)
matplot.early_plot_data(6, x, y)
x, y = pqfilereader.get_all_values(('d3p',), 1, 0, 26)
matplot.early_plot_data(6, x, y)

x, y = pqfilereader.get_all_values(('d1q',), 1, 0, 26)
matplot.early_plot_data(7, x, y)
x, y = pqfilereader.get_all_values(('d2q',), 1, 0, 26)
matplot.early_plot_data(7, x, y)
x, y = pqfilereader.get_all_values(('d3q',), 1, 0, 26)
matplot.early_plot_data(7, x, y)

########################################################################################################################
## Simulation
########################################################################################################################

# fixme check if this is useful, already declared in the __init__ file
Simulator.register_simulation_module(MinimalCyberPhysicalModule)
Simulator.register_simulation_module(CyberPhysicalModule)

# setup a simulation with a real time execution
sim = Simulator(RealTimeExecutionManager(int(args['time']) * units.seconds))
# add a regulation manager to the simulation
# with 3 cycles and a wait of 3 seconds between each steps
regulation = RegulationManager(3, 3, True)
# add a security manager to the simulation
security = SecurityManager()

########################################################################################################################
## Cyber Physical Device
########################################################################################################################

# open the district specified in the script parameter
# like -d a : open only district a or -d abc : open district a, b and c
district = list(args['district'])
for d in district:
    feeder = Feeder(d)
    if feeder.initialize_district():
        feeder.register_actor(pqfilereader) # dependency
        #feeder.register_actor(guicontroller)
        feeder.register_regulation(regulation)
        feeder.register_security(security)
        feeder.register_district(sim.cyberphysical)
        feeder.register_plot(matplot)
        feeder.register_plot(matplot2)

########################################################################################################################
## Activity Manager
########################################################################################################################

activity = ActivityManager()
activity.register_activity_listener('plot', matplot)

activity.start_activity_manager()

########################################################################################################################
## Start Simulation
########################################################################################################################

sim.cyberphysical.set_regulation_manager(regulation)
#sim.cyberphysical.set_security_manager(security)
sim.cyberphysical.add_module_listener(matplot)
sim.cyberphysical.add_module_listener(pqfilereader)
#sim.cyberphysical.add_module_listener(guicontroller)

sim.minimalcyberphysical.add_element(pqfilereader)

matplot.start_ploting()
# fixme support more than only one figure
# matplot2.start_ploting()

import time
start_time = time.time()
sim.reset()
print('start simulation')
sim.run(int(args['runtime']) * units.seconds, int(args['deltatime']) * units.seconds)
print('end simulation')
print 'total time:',time.time()-start_time

