"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridlab.actor.genericcallable import PQGenericCallable
from gridlab.actor.matlabactor import PQMatlab
from gridlab.gridlabcyberphysicalsystem.feeder import Feeder
from gridlab.optimisation.optimisation import VoltageOptimisation
from gridlab.paramtype import ParamType, SetPointType
from gridplot.plot.matplot import MatPlot
from gridsim.cyberphysical.actor.pqfilereader import PQFileReader
from gridsim.cyberphysical.security import SecurityManager
from gridsim.execution import RealTimeExecutionManager
from gridsim.simulation import Simulator
from gridsim.unit import units
import collections

########################################################################################################################
## Matlab Actor or Voltage optimisation
########################################################################################################################

matlab = False
with_load = True

pqactor = None

district = 'd'
sim_offset = 0
sim_length = 31
sim_step = 1
sim_time = 10

if matlab:
    # matlab read-write param are statically linked
    pqactor = PQGenericCallable(PQMatlab(), local_init=False)
    # request the first matlab script to get the simulation configuration
    district, sim_offset, sim_length, sim_step, sim_time = pqactor.init_callable()
else:
    pqactor = PQGenericCallable(VoltageOptimisation(district))
    pqactor.init_callable()

measure = collections.OrderedDict()
for i in range(0, 25):
    measure[(ParamType(i), district, 1)] = 0.
for i in range(0, 25):
    measure[(ParamType(i), district, 2)] = 0.
for i in range(0, 25):
    measure[(ParamType(i), district, 3)] = 0.
for i in range(0, 25):
    measure[(ParamType(i), district, 5)] = 0.

pqactor.init_order_measure(measure)

########################################################################################################################
## File Actor PV
########################################################################################################################

readparamfile = [(ParamType._PA,), (ParamType._QA,),
                 (ParamType._USUM,)]

writeparamfile = [(ParamType._P, district, 1),
                  (ParamType._P, district, 2),
                  (ParamType._P, district, 3)]

opcode = {'power1': (ParamType._P, district, 1),
          'power2': (ParamType._P, district, 2),
          'power3': (ParamType._P, district, 3)}

# sinus_profile_pv.csv input profile file
pv_file_reader = PQFileReader('pqfilereader', 'data/csv/test_pv.csv',
                              'logpv.csv',
                              readparamfile, writeparamfile)

pv_file_reader.initFile(opcode)

# link with changing value from the file
pv_file_reader.register_sync_value(pqactor, (ParamType._P, district, 1))
pv_file_reader.register_sync_value(pqactor, (ParamType._P, district, 2))
pv_file_reader.register_sync_value(pqactor, (ParamType._P, district, 3))

########################################################################################################################
## File Actor Load
########################################################################################################################

# sinus_profile_pv.csv input profile file
load_file_reader = PQFileReader('pqfilereader', 'data/csv/test_load.csv',
                                'logload.csv',
                                readparamfile, writeparamfile)

load_file_reader.initFile(opcode)

# link with changing value from the file
load_file_reader.register_sync_value(pqactor, (ParamType._P, district, 1))
load_file_reader.register_sync_value(pqactor, (ParamType._P, district, 2))
load_file_reader.register_sync_value(pqactor, (ParamType._P, district, 3))

list = collections.OrderedDict([((pv_file_reader, (ParamType._P, district, 1)), 0.),
                                ((pv_file_reader, (ParamType._P, district, 2)), 0.),
                                ((pv_file_reader, (ParamType._P, district, 3)), 0.),
                                ((load_file_reader, (ParamType._P, district, 1)), 0.),
                                ((load_file_reader, (ParamType._P, district, 2)), 0.),
                                ((load_file_reader, (ParamType._P, district, 3)), 0.)])

pqactor.init_order_input(list)

########################################################################################################################
## Plot
########################################################################################################################

matplot = MatPlot('Optimisation Reactive Power for Voltage stabilization', sim_offset)

matplot.create_plot_data('Act. P. [W]', [(ParamType._PA, district, 1),
                                         (ParamType._PA, district, 2),
                                         (ParamType._PA, district, 3),
                                         (ParamType._QA, district, 1),
                                         (ParamType._QA, district, 2),
                                         (ParamType._QA, district, 3)])

matplot.create_plot_data('Vol. Sum [V]', [(ParamType._USUM, 'd', 1),
                                          (ParamType._USUM, 'd', 2),
                                          (ParamType._USUM, 'd', 3),
                                          (ParamType._USUM, 'd', 5)])

matplot.create_plot_data('Phy', [(SetPointType._Cos_Phy_1, district, 1),
                                 (SetPointType._Cos_Phy_2, district, 2),
                                 (SetPointType._Cos_Phy_3, district, 3)])

# get data from file
x, y = pv_file_reader.get_all_values(('power1',), sim_step, sim_offset, sim_offset + sim_length)
# plot early data from file in the first and second figure
matplot.early_plot_data(0, x, y)

pqactor.register_sync_value(matplot, (SetPointType._Cos_Phy_1, district, 1))
pqactor.register_sync_value(matplot, (SetPointType._Cos_Phy_2, district, 2))
pqactor.register_sync_value(matplot, (SetPointType._Cos_Phy_3, district, 3))

########################################################################################################################
## Simulation
########################################################################################################################

sim = Simulator(RealTimeExecutionManager(sim_time * units.seconds))
# add a security manager to the simulation
security = SecurityManager()

########################################################################################################################
## Cyber Physical Device
########################################################################################################################

# create a new feeder and register actors on it
feeder = Feeder(district)
feeder.initialize_district()

feeder.register_actor(pv_file_reader)
if with_load:
    feeder.register_actor(load_file_reader)
feeder.register_actor(pqactor)
feeder.register_security(security)
feeder.register_district(sim.cyberphysical)
feeder.register_plot(matplot)

########################################################################################################################
## Start Simulation
########################################################################################################################

# sim.cyberphysical.set_security_manager(security)

sim.cyberphysical.add_module_listener(matplot)
sim.cyberphysical.add_module_listener(pv_file_reader)
if with_load:
    sim.cyberphysical.add_module_listener(load_file_reader)
sim.cyberphysical.add_module_listener(pqactor)

sim.minimalcyberphysical.add_element(pv_file_reader)
if with_load:
    sim.minimalcyberphysical.add_element(load_file_reader)
sim.minimalcyberphysical.add_element(pqactor)

matplot.start_ploting()

sim.reset(sim_offset * units.seconds)
print('start simulation')
sim.run(sim_length * units.seconds, sim_step * units.seconds)
print('end simulation')
