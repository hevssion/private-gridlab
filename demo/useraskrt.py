"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

import argparse
import os

from demo.pqcontroller import PQController
from gridlab.gridlabcyberphysicalsystem.feeder import Feeder
from gridlab.paramtype import ParamType
from gridsim.cyberphysical.simulation import CyberPhysicalModule
from gridsim.execution import RealTimeExecutionManager
from gridsim.simulation import Simulator
from gridsim.unit import units

# Example of a user input terminal reader using the cyber-physical module with the following starting params
# -d b -t 5 -rt 50 -dt 1

parser = argparse.ArgumentParser(description='Simulate the content of a file over the Gridlab District')
parser.add_argument('-o', '--outfile', help='Output file for the logging values, (current,voltage,...)', required=False)

parser.add_argument('-d', '--district', help='Specify the district number', required=True)

parser.add_argument('-dt', '--deltatime', help='Time interval for the simulation', required=True)
parser.add_argument('-rt', '--runtime', help='Total run time', required=True)
parser.add_argument('-t', '--realtime', help='Real time for the simulation', required=True)

args = vars(parser.parse_args())
print os.getcwd()

if 'outfile' not in args.keys() or args['outfile'] == None:
    args['outfile'] = os.getcwd() + '\log.csv'

########################################################################################################################
## User Actor
########################################################################################################################

readparam = [(ParamType._PA,), (ParamType._QA,)]

writeparam = [(ParamType._P, args['district'], 1), (ParamType._Q, args['district'], 1)]

opcode = {'a1p': (ParamType._P, 'a', 1), 'a1q': (ParamType._Q, 'a', 1),
          'a2p': (ParamType._P, 'a', 2), 'a2q': (ParamType._Q, 'a', 2),
          'a3p': (ParamType._P, 'a', 3), 'a3q': (ParamType._Q, 'a', 3),
          'b1p': (ParamType._P, 'b', 1), 'b1q': (ParamType._Q, 'b', 1),
          'b2p': (ParamType._P, 'b', 2), 'b2q': (ParamType._Q, 'b', 2),
          'b3p': (ParamType._P, 'b', 3), 'b3q': (ParamType._Q, 'b', 3),
          'c1p': (ParamType._P, 'c', 1), 'c1q': (ParamType._Q, 'c', 1),
          'c2p': (ParamType._P, 'c', 2), 'c2q': (ParamType._Q, 'c', 2),
          'c3p': (ParamType._P, 'c', 3), 'c3q': (ParamType._Q, 'c', 3),
          'd1p': (ParamType._P, 'd', 1), 'd1q': (ParamType._Q, 'd', 1),
          'd2p': (ParamType._P, 'd', 2), 'd2q': (ParamType._Q, 'd', 2),
          'd3p': (ParamType._P, 'd', 3), 'd3q': (ParamType._Q, 'd', 3)}

pqcontroller = PQController(readparam, writeparam)

pqcontroller.initConsole(opcode)

########################################################################################################################
## Simulation
########################################################################################################################

Simulator.register_simulation_module(CyberPhysicalModule)
sim = Simulator(RealTimeExecutionManager(int(args['realtime']) * units.seconds))

########################################################################################################################
## Cyber Physical Device
########################################################################################################################

feeder = Feeder(args['district'])
if feeder.initialize_district():
    feeder.register_actor(pqcontroller)
    feeder.register_district(sim.cyberphysical)

########################################################################################################################
## Start Simulation
########################################################################################################################

sim.cyberphysical.add_module_listener(pqcontroller)
sim.minimalcyberphysical.add_element(pqcontroller)

sim.reset()
print('start simulation')
sim.run(int(args['runtime']) * units.seconds, int(args['deltatime']) * units.seconds)
print('end simulation')
