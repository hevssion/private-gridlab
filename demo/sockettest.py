"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridlab.actor.genericcallable import PQGenericCallable
from gridlab.actor.socketactor import PQSocket
from gridlab.gridlabcyberphysicalsystem.feeder import Feeder
from gridlab.paramtype import ParamType
from gridplot.plot.matplot import MatPlot
from gridsim.execution import RealTimeExecutionManager
from gridsim.simulation import Simulator
from gridsim.unit import units

########################################################################################################################
## Matlab Actor
########################################################################################################################

district = 'd'
sim_offset = 28800
sim_length = 65400 - 28800
sim_step = 600
sim_time = 10

read_params = [(ParamType._PL1,), (ParamType._PL2,), (ParamType._PL3,), (ParamType._PA,),
               (ParamType._QL1,), (ParamType._QL2,), (ParamType._QL3,), (ParamType._QA,),
               (ParamType._SL1,), (ParamType._SL2,), (ParamType._SL3,), (ParamType._SA,)]

write_params = [(ParamType._P, district, 1),
                (ParamType._P, district, 2),
                (ParamType._P, district, 3)]

protocol = {'a1p': (ParamType._P, 'a', 1), 'a1q': (ParamType._Q, 'a', 1),
            'a2p': (ParamType._P, 'a', 2), 'a2q': (ParamType._Q, 'a', 2),
            'a3p': (ParamType._P, 'a', 3), 'a3q': (ParamType._Q, 'a', 3),
            'b1p': (ParamType._P, 'b', 1), 'b1q': (ParamType._Q, 'b', 1),
            'b2p': (ParamType._P, 'b', 2), 'b2q': (ParamType._Q, 'b', 2),
            'b3p': (ParamType._P, 'b', 3), 'b3q': (ParamType._Q, 'b', 3),
            'c1p': (ParamType._P, 'c', 1), 'c1q': (ParamType._Q, 'c', 1),
            'c2p': (ParamType._P, 'c', 2), 'c2q': (ParamType._Q, 'c', 2),
            'c3p': (ParamType._P, 'c', 3), 'c3q': (ParamType._Q, 'c', 3),
            'd1p': (ParamType._P, 'd', 1), 'd1q': (ParamType._Q, 'd', 1),
            'd2p': (ParamType._P, 'd', 2), 'd2q': (ParamType._Q, 'd', 2),
            'd3p': (ParamType._P, 'd', 3), 'd3q': (ParamType._Q, 'd', 3)}

# matlab read-write param are statically linked
pqactor = PQGenericCallable(PQSocket(5555, protocol, district), read_params=read_params, write_params=write_params)
# request the first matlab script to get the simulation configuration
pqactor.init_callable()

########################################################################################################################
## File Actor
########################################################################################################################

readparamfile = [(ParamType._PA,), (ParamType._QA,),
                 (ParamType._USUM,)]

writeparamfile = [(ParamType._P, district, 1),
                  (ParamType._P, district, 2),
                  (ParamType._P, district, 3)]

opcode = {'power1': (ParamType._P, district, 1),
          'power2': (ParamType._P, district, 2),
          'power3': (ParamType._P, district, 3)}


########################################################################################################################
## Plot
########################################################################################################################

matplot = MatPlot('Optimisation Reactive Power for stabilize the Voltage', sim_offset)

matplot.create_plot_data('Vol. Sum [V]', [(ParamType._USUM, 'd', 1),
                                          (ParamType._USUM, 'd', 2),
                                          (ParamType._USUM, 'd', 3)])

########################################################################################################################
## Simulation
########################################################################################################################

sim = Simulator(RealTimeExecutionManager(sim_time * units.seconds))

########################################################################################################################
## Cyber Physical Device
########################################################################################################################

feeder = Feeder(district)
feeder.initialize_district()
feeder.register_actor(pqactor)
feeder.register_district(sim.cyberphysical)
feeder.register_plot(matplot)

########################################################################################################################
## Start Simulation
########################################################################################################################

sim.cyberphysical.add_module_listener(pqactor)
sim.minimalcyberphysical.add_element(pqactor)

matplot.start_ploting()

sim.reset(sim_offset * units.seconds)
print('start simulation')
sim.run(sim_length * units.seconds, sim_step * units.seconds)
print('end simulation')
