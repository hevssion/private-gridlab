"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from genericcallable import GenericCallable

from gridlab.paramtype import ParamType, SetPointType

import matlab.engine

from scipy import *


class PQMatlab(GenericCallable):
    def __init__(self, district=''):
        super(PQMatlab, self).__init__(district)

        self._engine = None
        self._debug_phy = [0,0,0]

    def init_callable(self):
        self._engine = matlab.engine.start_matlab()
        self.district, sim_offset, sim_length, sim_step, sim_time = getattr(self._engine,
                                                                            'simulation_initialize')(
            nargout=5)
        self.district = str(self.district)

        self.read_params = [(ParamType._U1,), (ParamType._U2,), (ParamType._U3,), (ParamType._UN,),
                            (ParamType._I1,), (ParamType._I2,), (ParamType._I3,), (ParamType._IN,),
                            (ParamType._UL12,), (ParamType._UL23,), (ParamType._UL31,), (ParamType._USUM,),
                            (ParamType._ISUM,),
                            (ParamType._PL1,), (ParamType._PL2,), (ParamType._PL3,), (ParamType._PA,),
                            (ParamType._QL1,), (ParamType._QL2,), (ParamType._QL3,), (ParamType._QA,),
                            (ParamType._SL1,), (ParamType._SL2,), (ParamType._SL3,), (ParamType._SA,)]

        self.write_params = [(ParamType._Q, self.district, 1),
                             (ParamType._Q, self.district, 2),
                             (ParamType._Q, self.district, 3),
                             (SetPointType._Cos_Phy_1, self.district, 1),
                             (SetPointType._Cos_Phy_2, self.district, 2),
                             (SetPointType._Cos_Phy_3, self.district, 3)]

        return self.district, sim_offset, sim_length, sim_step, sim_time

    def send_request_data(self, step, setpoint, measure):

        print step
        if step > 15:
            return {(ParamType._Q, self.district, 1): 0,
                    (ParamType._Q, self.district, 2): 0,
                    (ParamType._Q, self.district, 3): 0}

        if self._engine is not None:
            res = getattr(self._engine, 'simulation_step')(step, setpoint.values(), measure.values(), nargout=3)

            # self._debug_phy[0] = res[0]
            # self._debug_phy[1] = res[1]
            # self._debug_phy[2] = res[2]
            #
            # q_inv_a = -setpoint.values()[0] * tan(res[0])
            # q_inv_b = -setpoint.values()[1] * tan(res[1])
            # q_inv_c = -setpoint.values()[2] * tan(res[2])

            return {(ParamType._Q, self.district, 1): res[0], (ParamType._Q, self.district, 2): res[1],
                    (ParamType._Q, self.district, 3): res[2]}
        else:
            raise Exception('Engine in PQMatlab not instantiate')

    def debug_request_data(self):
        # return debug array filled to provide information about the debug
        return {(SetPointType._Cos_Phy_1, self.district, 1): self._debug_phy[0],
                (SetPointType._Cos_Phy_2, self.district, 2): self._debug_phy[1],
                (SetPointType._Cos_Phy_3, self.district, 3): self._debug_phy[2]}

    def get_name(self):
        return 'pqmatlab' + self.district
