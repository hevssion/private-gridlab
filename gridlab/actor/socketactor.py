"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from genericcallable import GenericCallable

import socket
import json


class PQSocket(GenericCallable):
    def __init__(self, port, protocol, district=''):
        super(PQSocket, self).__init__(district)

        self._server = None
        self._server_address = ('', port)

        self._client = None

        self._protocol = protocol

    def init_callable(self):
        self._server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server.bind(self._server_address)
        self._server.listen(1)
        self._client, client_address = self._server.accept()
        self._client.settimeout(5)  # set 5 seconds timeout

        try:
            self._client.sendall('hello client')
        except Exception:
            raise Exception('client disconnected')

    def end_callable(self):
        if self._client is not None:
            self._client.close()

    def send_request_data(self, step, setpoint, measure):
        try:
            self._client.sendall(json.dumps(measure))
        except Exception:
            pass
        try:
            data = self._client.recv(1024)
            res = {}
            try:
                buffer = json.loads(data)
                for k,v in buffer.items():
                    if str(k) in self._protocol.keys():
                        res[self._protocol[str(k)]] = v
                return res
            except ValueError:
                print 'Json socket value error'
                return {}
        # decode key value
        except socket.timeout:
            return {}
        except socket.error:
            raise Exception('client disconnected')

    def get_name(self):
        return 'pqmatlab' + self.district
