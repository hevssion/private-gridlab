"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridsim.core import AbstractSimulationElement
from gridsim.cyberphysical.element import Actor
from gridsim.cyberphysical.simulation import CyberPhysicalModuleListener

from gridsim.decorators import accepts, returns

from collections import OrderedDict


class GenericCallable(object):
    @accepts((1, (str)))
    def __init__(self, district):
        super(GenericCallable, self).__init__()

        self.district = district

        self.read_params = None
        self.write_params = None

    def init_callable(self):
        pass

    def end_callable(self):
        pass

    @accepts((1, 2), (float, int), (3, list))
    @returns((int, float))
    def send_request_data(self, step, setpoint, measure):
        raise NotImplementedError('Pure abstract method!')

    def debug_request_data(self):
        pass

    @returns(str)
    def get_name(self):
        raise NotImplementedError('Pure abstract method!')


class PQGenericCallable(Actor, AbstractSimulationElement, CyberPhysicalModuleListener):
    @accepts((1, GenericCallable), ((2, 3, 4), (int, float)), ((5, 6), list))
    def __init__(self, generic_callable, local_init=True,
                 read_params=None, write_params=None, sync_params=None):
        """
        __init__(self, generic_callable, local_init, read_params, write_params)

        PQGenericCallable class provide a way to hold all the notified values before the call of the
        GenericCallable that does the request for the data to write with the data read from the devices.
        This class can be initialized by the main script or by the GenericCallable it-self.
        The GenericCallable can provide

        :param generic_callable: GenericCallable to link with this class
        :param local_init: the initialization should be local from the main file, or remote from the GenericCallable it-self
            this should return a packed values @see function init_callable
        :param read_params: list of read param to register on
        :param write_params: list of write param to register on
        """
        Actor.__init__(self)
        AbstractSimulationElement.__init__(self, generic_callable.get_name())
        CyberPhysicalModuleListener.__init__(self)

        self.read_params = read_params
        self.write_params = write_params
        self.sync_params = sync_params

        self._generic_callable = generic_callable

        self._local_init = local_init

        self._district = None
        self._offset = 0
        self._sim_length = 0
        self._sim_step = 0
        self._sim_time = 0

        self._hold_read_param = OrderedDict()
        self._hold_listen_param = OrderedDict()
        # replace
        self._hold_listen_param_actor = {}
        self._hold_debug_param = OrderedDict()
        self._hold_write_param = {}

        self._sim_current_time = 0

    def init_callable(self):
        if self._local_init:  # configuration is done by the generic callable
            self._generic_callable.init_callable()
        else:  # get configuration from the actor
            self._district, self._offset, self._sim_length, self._sim_step, self._sim_time = self._generic_callable.init_callable()
            self.friendly_name = self._generic_callable.get_name() + self._district
        # get read-write param from the Generic Callable if it's not yet defined
        if self.read_params is None:
            self.read_params = self._generic_callable.read_params
        if self.write_params is None:
            self.write_params = self._generic_callable.write_params

        return self._district, self._offset, self._sim_length, self._sim_step, self._sim_time

    def reset(self):
        pass

    def update(self, time, delta_time):
        self._sim_current_time = time

    def calculate(self, time, delta_time):
        pass

    def init_order_input(self, list_order):
        self._hold_listen_param = list_order

    def init_order_measure(self, list_order):
        self._hold_read_param = list_order

    def sync_value_define(self, actor, sync_value):
        """
        register_input_sync_value_actor(self, actor, sync_value)

        Initialize the dict

        :param actor:
        :param sync_value:
        :return:
        """
        self._hold_listen_param_actor[actor] = sync_value

    def read_param_define(self, cps, define_read_param):
        """
        read_param_define(self, define_read_param)

        :param cps:
        :param define_read_param:
        :return:
        """
        pass
        # self._hold_read_param[cps] = define_read_param

    def on_sync_value(self, callable, write_param, data):
        self._hold_listen_param[(callable, write_param)] = data
        # keep in order
        # TODO self._hold_listen_param_actor[callable][write_param] = data

    def notify_read_param(self, read_param, data):
        self._hold_read_param[read_param] = data
        # TODO self._hold_read_param[read_param[2]][read_param] = data

    def cyberphysical_write_begin(self):
        if len(self._hold_read_param) is not 0:
            temp = self._generic_callable.send_request_data(self._sim_current_time,
                                                            self._hold_listen_param,
                                                            self._hold_read_param)
            self._hold_debug_param = self._generic_callable.debug_request_data()
            if len(temp) is not 0:
                self._hold_write_param = temp

    # def prepare_get_value(self, write_param):
    #     self._hold_debug_param[write_param] = None

    def get_debug_value(self, write_param):
        if write_param in self._hold_debug_param.keys():
            return self._hold_debug_param[write_param]
        return 0

    def get_value(self, write_param):
        # check the write_param before saving the Q
        # print write_param, self._q.keys()
        if write_param in self._hold_write_param.keys():
            return float(self._hold_write_param[write_param])
        return 0

    def cyberphysical_module_end(self):
        self._generic_callable.end_callable()
