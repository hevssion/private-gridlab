from gridsim.cyberphysical.element import Actor
from gridsim.cyberphysical.simulation import CyberPhysicalModuleListener

from gridlab.paramtype import ParamType

import sys
from PyQt4 import QtGui, QtCore

import threading
import time

class Window(QtGui.QWidget):
    def __init__(self, listener):
        QtGui.QWidget.__init__(self)

        self._listener = listener

        self.p_linedit = QtGui.QLineEdit('',self)
        self.q_linedit = QtGui.QLineEdit('', self)

        self.p_linedit.editingFinished.connect(self.p_change)
        self.q_linedit.editingFinished.connect(self.q_change)

        p_label = QtGui.QLabel('Active Power [W]',self)
        q_label = QtGui.QLabel('Reactive Power [Var]',self)

        layout = QtGui.QVBoxLayout(self)
        p_layout = QtGui.QHBoxLayout()
        q_layout = QtGui.QHBoxLayout()

        layout.addLayout(p_layout)
        layout.addLayout(q_layout)

        p_layout.addWidget(p_label)
        p_layout.addWidget(self.p_linedit)
        q_layout.addWidget(q_label)
        q_layout.addWidget(self.q_linedit)

        self.button = QtGui.QPushButton('Send')
        self.button.clicked.connect(self.handleButton)

        layout.addWidget(self.button)

    def p_change(self):
        try:
            temp = int(self.p_linedit.text())
        except ValueError:
            print 'Error format value, should be an integer'
            return
        print 'p', temp
        self._listener.new_gui_values('p', temp)

    def q_change(self):
        try:
            temp = int(self.q_linedit.text())
        except ValueError:
            print 'Error format value, should be an integer'
            return
        print 'q', temp
        self._listener.new_gui_values('q', temp)

    def handleButton(self):
        print ('Debug message')

class PQGuiController(Actor, CyberPhysicalModuleListener):
    def __init__(self, read_params, write_params):
        Actor.__init__(self)
        CyberPhysicalModuleListener.__init__(self)

        self.read_params = read_params
        self.write_params = write_params

        self._p = 0
        self._q = 0

        self._thread = None

    def start_controller(self):
        if self._thread == None:
            self._thread = threading.Thread(target=self.runtime)
            self._thread.start()

    def runtime(self):
        app = QtGui.QApplication(sys.argv)
        window = Window(self)
        window.setWindowTitle('GridCtrl')
        window.show()
        app.exec_()

    def new_gui_values(self, type, value):
        if type == 'p':
            self._p = value
        elif type == 'q':
            self._q = value

    def notify_read_param(self, read_param, data):
        pass

    def get_value(self, write_param):
        print 'guiactor',write_param
        if write_param[0] == ParamType._P:
            return self._p
        elif write_param[0] == ParamType._Q:
            return self._q
        return 0

def start_window1(listener):
    thread = threading.Thread(target=listener.start_controller)
    thread.start()

keep_thread = 1
def start_window(listener):
    global keep_thread

    while True:
        app = QtGui.QApplication(sys.argv)
        window = Window(listener)
        window.setWindowTitle('GridCtrl')
        window.show()
        app.exec_()

        raise Exception('Windows Closed')

        while keep_thread == 0:
            time.sleep(1)