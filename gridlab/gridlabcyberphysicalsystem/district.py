"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from pyModbusTCP.client import ModbusClient
from pyModbusTCP.constants import MODBUS_RTU

from gridlab.modbus.modbus import ModBusOverTcp
from gridlab.paramtype import ParamType

from collections import OrderedDict

# defined know host in the gridlab
defined_host = {
    'a': {'acs800': ('153.109.14.46', 502), 'sicam': ('153.109.14.47', 502), 'svc': ('153.109.14.48',100)},
    'b': {'acs800': ('153.109.14.51', 502), 'sicam': ('153.109.14.52', 502), 'svc': ('153.109.14.53',100)},
    'c': {'acs800': ('153.109.14.56', 502), 'sicam': ('153.109.14.57', 502), 'svc': ('153.109.14.58',100)},
    'd': {'acs800': ('153.109.14.61', 502), 'sicam': ('153.109.14.62', 502), 'svc': ('153.109.14.63',100)}
}

# redefine in a list order the available district
existing_district_name = ['a', 'b', 'c', 'd']

# sicam get data mode
register_type = {'measured_values': 1, 'harmonic_1': 2, 'harmonic_2': 4, 'energy_values': 8}


# output console color
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# don't connect to the physical device and simulate a fake one in the program if True
# else connect to the correct physical device through modbus (tcp/rtu)
DEGUC_COM = False


class DisctrictInitializer(object):
    district = {}

    @staticmethod
    def initialize_host(district_name, host=None):
        """
        initializeHost(districtnams,host)

        Initialize the connection with the gridlab district

        :param district_name: district letter
        :param host: optional, tuple with address and port for the host
        :return: the corresponding socket connection to the house
        """
        if host is None:
            if district_name in defined_host.keys():
                host = defined_host[district_name]
            else:
                return None
                # raise Exception('Host not found')

        if district_name in DisctrictInitializer.district.keys():
            return DisctrictInitializer.district[district_name]

        # attempt to connect to physical device
        con = {}
        for n, h in host.items():
            if len(h) is 2:  # need to have address and port
                if n in 'acs800':
                    modbus_tcp = ModbusClient(host=h[0], port=h[1])
                    modbus_tcp.auto_open(True)
                    con['acs800'] = modbus_tcp
                    if con['acs800'].open() is False:
                        raise Exception('Cannot connect to acs800')
                elif n in 'sicam':
                    modbus_over_tcp = ModBusOverTcp(host=h[0], port=h[1])
                    # modbus_over_tcp.auto_open(True)
                    con['sicam'] = modbus_over_tcp
                    if con['sicam'].open() is False:
                        raise Exception('Cannot connect to sicam')
        if 'sicam' not in con or 'acs800' not in con:
            raise Exception('sicam or acs800 modbus not initialized')

        # register the current created connection
        DisctrictInitializer.district[district_name] = con
        return con

    @staticmethod
    def initialize_read_register_param(district_name, house_number, register_range=1):
        """
        initializeReadRegisterParam(districtname,housename)

        Generic read parameter for the district initializer

        :param district_name: district letter
        :param housename: id of the slave house
        :param register_range: register range to read
        :return: tuple with salve address, register offset with length and ParamType
        """
        slave = 0

        # construct the physical register
        # starting at 40201 with a length if ParamType.getQuantity()
        register = []
        if register_range & 0b0001:
            register.append(
                {'all': 40201, 'quantity': ParamType.get_quantity(), 'offset': ParamType.get_offset()})
        if register_range & 0b0010 >> 1:
            pass
        if register_range & 0b0100 >> 2:
            pass
        if register_range & 0b1000 >> 3:
            register.append(
                {'all': 606 + 40201, 'quantity': ParamType.get_quantity(8), 'offset': ParamType.get_offset(8)})

        # create read paramtype for the gridlabcyberphysicalsystem
        param_type = []
        for reg in register:
            for i in range(0, reg['quantity']):
                param_type.append(ParamType(reg['offset'] + i))

        # get the salve address
        if district_name in existing_district_name:
            slave = (existing_district_name.index(district_name) + 1) * 10 + house_number
        else:
            raise Exception('No district found!')

        return slave, register, param_type

    @staticmethod
    def initialize_write_register_param(house_number):
        """
        initializeWriteRegisterParam(housenumber)

        Generic write parameter for districts initializer

        :param house_number: id of the house
        :return: register offset with corresponding writeparam
        """
        register = {}  # None
        if house_number is 1:
            register = {ParamType._P: 1066 - 1, ParamType._Q: 1063 - 1}  # 1-2 drive
        elif house_number is 2:
            register = {ParamType._P: 1084 - 1, ParamType._Q: 1081 - 1}  # 3-4 drive
        elif house_number is 3:
            register = {ParamType._P: 1102 - 1, ParamType._Q: 1099 - 1}  # 5-6 drive

        return register

    @staticmethod
    def initialize_reset_register_param(house_number):
        """
        initialize_reset_register_param(house_number)

        Initialize registers param to reset the physical devices, only district a should be reset, this is
        a known fault error at boot time.

        :param house_number: specify the house to reset
        :return: reset registers
        """
        register = OrderedDict()
        if house_number is 'a':
            register = [
                OrderedDict([(1088 - 1, 9), (1094 - 1, 1), (64 - 1, 1)]), #drive5
                OrderedDict([(1097 - 1, 9), (1103 - 1, 1), (72 - 1, 1)]), #6
                OrderedDict([(1070 - 1, 9), (1076 - 1, 1), (46 - 1, 1)]), #3
                OrderedDict([(1079 - 1, 9), (1085 - 1, 1), (55 - 1, 1)]), #4
                OrderedDict([(1052 - 1, 9), (1058 - 1, 1), (28 - 1, 1)]), #1
                OrderedDict([(1061 - 1, 9), (1067 - 1, 1), (37 - 1, 1)]), #2

                OrderedDict([(1088 - 1, 137), (1094 - 1, 1), (64 - 1, 1)]),
                OrderedDict([(1097 - 1, 137), (1103 - 1, 1), (72 - 1, 1)]),
                OrderedDict([(1070 - 1, 137), (1076 - 1, 1), (46 - 1, 1)]),
                OrderedDict([(1079 - 1, 137), (1085 - 1, 1), (55 - 1, 1)]),
                OrderedDict([(1052 - 1, 137), (1058 - 1, 1), (28 - 1, 1)]),
                OrderedDict([(1061 - 1, 137), (1067 - 1, 1), (37 - 1, 1)]),

                # {1088 - 1: (137,9), 1094 - 1: 1, 64 - 1: 1},
                # {1097 - 1: (137,9), 1103 - 1: 1, 72 - 1: 1},
                # {1070 - 1: (137,9), 1076 - 1: 1, 46 - 1: 1},
                # {1079 - 1: (137,9), 1085 - 1: 1, 55 - 1: 1},
                # {1052 - 1: (137,9), 1058 - 1: 1, 28 - 1: 1},
                # {1061 - 1: (137,9), 1067 - 1: 1, 37 - 1: 1}
            ]

        return register

    @staticmethod
    def deinitialize_host(district_name):
        """
        deinitialize_host(district_name)

        Load shutdown register to stop the district and all the houses under it.
        Does not work! Its not possible to shutdown the house except with the physical button

        :param district_name: district name to shutdown
        :return: shutdown register
        """

        # 07.01 MAIN CONTROL WORD
        # view page 87 data word (fieldbus control)
        # 0bit Start charging (close charging contactor).
        # 3bit Start modulation.
        # 7bit Reset.

        # 1088 - 1: 9

        # 98.01 COMMAND SEL
        # view page 69 option modules (actual signals and parameters)
        # I/O Through digital input terminals 1

        # 1094 - 1: 1

        # 08.01 MAIN STATUS WORD
        # view page 88

        # 64 - 1: 1

        register = OrderedDict()
        if district_name is 'a':
            register = [
                # drive 5
                {1088 - 1: 9, 1094 - 1: 1, 64 - 1: 1},
                # drive 6
                {1097 - 1: 9, 1103 - 1: 1, 72 - 1: 1},
                # drive 3
                {1070 - 1: 9, 1076 - 1: 1, 46 - 1: 1},
                # drive 4
                {1079 - 1: 9, 1085 - 1: 1, 55 - 1: 1},
                # drive 1
                {1052 - 1: 9, 1058 - 1: 1, 28 - 1: 1},
                # drive 2
                {1061 - 1: 9, 1067 - 1: 1, 37 - 1: 1}
            ]

        return register

    def __init__(self):
        super(DisctrictInitializer, self).__init__()
