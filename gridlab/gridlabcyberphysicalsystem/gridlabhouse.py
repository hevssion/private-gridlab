"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

import random

from gridlab.gridlabcyberphysicalsystem.district import DisctrictInitializer, DEGUC_COM
from gridlab.modbus.modbus import ModBusOverTcp
from gridlab.paramtype import ParamType, SetPointType, ConverterType
from gridlab.regulation.pidregulator import PIDRegulator
from gridplot.connector.plotconnector import AbstractPlotSystem, PlotParam
from gridsim.cyberphysical.core import WriteParam, ReadParam
from gridsim.cyberphysical.element import AbstractCyberPhysicalSystem, Actor
from gridsim.cyberphysical.security import Security
from gridsim.cyberphysical.simulation import CyberPhysicalModuleListener
from gridsim.decorators import accepts

import time


class GridLabHouse(AbstractCyberPhysicalSystem, CyberPhysicalModuleListener, AbstractPlotSystem, Security):
    def __init__(self, friendly_name, converters=None):
        """
        __init__(self,friendly_name)

        This class communicate using modbus protocol to the asc800 drives and sicam meters

        :param friendly_name: name given to the gridsim Element
        """
        if DEGUC_COM:
            converters = None
        AbstractCyberPhysicalSystem.__init__(self, friendly_name, converters)
        CyberPhysicalModuleListener.__init__(self)
        AbstractPlotSystem.__init__(self)
        Security.__init__(self)

        # ReadParam list supported
        self.rlist = None
        # WriteParam list supported
        self.wlist = None

        self._district_name = ''
        self._house_name = 0

        # Technical information to exchange data over modbus
        self._read_mode = 9
        self._sicam_register = []

        self._acs800_register = {}
        self._acs800_register_reset = {}

        # avoid rewrite same value without a change between
        self._old_write = {}

        # Id of the district
        self._slave = 0
        self._con = None

        self._pid = {}
        self._single_write_register = {}

    def initialize(self, district_name, house_number,
                   read_params=None, sicam_register=None, slave=None,
                   write_params=None, acs800_register=None):
        """
        initialize(self, readparamlist, sicamregister, writeparamlist, acs800register, slave)

        Initialize the ReadParam and the WriteParam

        :param house_number: id of the slave house
        :param district_name: the district letter
        :param read_params: readparam list supported
        :param sicam_register: sicam modbus technical information
        :param write_params: writeparam list supported, (required)
        :param acs800_register: acs800 modbus technical information
        """
        self._district_name = district_name

        # Connection
        ################################################################################################################
        if not DEGUC_COM:
            self._con = DisctrictInitializer.initialize_host(district_name)
            if self._con is None:
                raise Exception('Error no district name found')

        # Read Param Config
        ################################################################################################################
        self._slave = slave
        self.rlist = read_params
        self._sicam_register = sicam_register
        self._house_name = house_number

        # get the default value if not provided for slave, sicamregister and readparamlist
        if slave is None or read_params is None or sicam_register is None:
            self._slave, self._sicam_register, self.rlist = DisctrictInitializer.initialize_read_register_param(
                district_name, house_number, self._read_mode)

        # TODO check the number of read params list
        # if 'quantity' not in self._sicam_register or self._sicam_register['quantity'] is not len(self.rlist):
        #    raise Exception('ReadParam does not match the registers value')

        # create read param list for the cyber physical system
        for r in self.rlist:
            self.read_params.append(ReadParam(r, (r, district_name, house_number)))

        # Write Param Config
        ################################################################################################################
        self.wlist = write_params
        self._acs800_register = acs800_register

        # get the default value if not provided for asc800register
        if acs800_register is None:
            self._acs800_register = DisctrictInitializer.initialize_write_register_param(house_number)

        # TODO check the number of write params list
        if self.wlist is not None:
            if len(self._acs800_register) is not len(self.wlist):
                raise Exception('WriteParam does not match the registers value')

            # create wirteparam list for the cyberphysical system
            for w in self.wlist:
                if len(w) >= 2:
                    self.write_params.append(WriteParam(w[0], (w[0], district_name, house_number), w[1]))
                else:
                    raise Exception('Error WriteParam cannot be created')

        # Write Debug Param
        ################################################################################################################
        self.debug_write_params.append(
            WriteParam(SetPointType._Cos_Phy_1, (SetPointType._Cos_Phy_1, district_name, house_number)))
        self.debug_write_params.append(
            WriteParam(SetPointType._Cos_Phy_2, (SetPointType._Cos_Phy_2, district_name, house_number)))
        self.debug_write_params.append(
            WriteParam(SetPointType._Cos_Phy_3, (SetPointType._Cos_Phy_3, district_name, house_number)))

        self.debug_write_params.append(
            WriteParam(SetPointType._simple_test1, (SetPointType._simple_test1, district_name, house_number)))
        self.debug_write_params.append(
            WriteParam(SetPointType._simple_test2, (SetPointType._simple_test2, district_name, house_number)))

        # Add Plot
        ################################################################################################################
        for r in self.rlist:
            self.plot_measures.append(PlotParam((r, district_name, house_number)))
        self.plot_measures_write.append(PlotParam((ParamType._Q, district_name, house_number)))
        self.plot_measures_write.append(PlotParam((ParamType._P, district_name, house_number)))

        # Add Reset
        ################################################################################################################
        self._acs800_register_reset = DisctrictInitializer.initialize_reset_register_param(district_name)
        # Add Regulator
        ################################################################################################################
        self._pid[ParamType._P] = PIDRegulator(0.5, 0.3, 0.2, 1)
        self._pid[ParamType._Q] = PIDRegulator(0.5, 0.3, 0.2, 1)

        # init single write register P and Q
        self._single_write_register[ParamType._P] = 40233
        self._single_write_register[ParamType._Q] = 40241

    @accepts((1, Actor))
    def add(self, actor):
        rparamlist = actor.read_params
        wparamlist = actor.write_params

        # TODO change loop order
        # register write actors
        for w in self.write_params:
            for a in wparamlist:
                if a == w.info:
                    w.add_callable(actor)
                elif len(a) is 1 and a[0] is w.write_param:
                    w.add_callable(actor)
        # register debug write actors
        for w in self.debug_write_params:
            for a in wparamlist:
                if a == w.info:
                    w.add_callable(actor)
                elif len(a) is 1 and a[0] is w.write_param:
                    w.add_callable(actor)
        # register read actors
        for r in self.read_params:
            for a in rparamlist:
                if a is r.info:
                    r.add_listener(actor)
                elif len(a) is 1 and a[0] is r.read_param:
                    r.add_listener(actor)

        actor.id = len(self.actors)
        self.actors.append(actor)
        return actor

    ####################################################################################################################
    ## Module
    ####################################################################################################################

    def cyberphysical_module_begin(self):
        # reset communication with district 'a'
        if not DEGUC_COM:
            for drive in self._acs800_register_reset:
                print drive
                for reg, val in drive.items():
                    if isinstance(val, tuple):
                        for v in val:
                            if (reg > 1000):
                                self._con['acs800'].write_multiple_registers(reg, [v])
                            else:
                                self._con['acs800'].read_holding_registers(reg, v)
                    else:
                        if (reg > 1000):
                            self._con['acs800'].write_multiple_registers(reg, [val])
                        else:
                            print self._con['acs800'].read_holding_registers(reg, val)

                    time.sleep(0.1)

        print "reset done"

    def cyberphysical_module_end(self):
        # end the district
        pass

    ####################################################################################################################
    ## Physical Device
    ####################################################################################################################

    def physical_write_params(self, write_params):
        """
        physical_write_params(self,write_params)

        Write params to the system using modbus TCP.
        Check if data are not the same than the last time, and write to the system

        :param write_params: dict of datas to write with id
        """

        self.update_plot_setpoint(write_params.values()[:])
        if DEGUC_COM:
            print 'physical_write_params', self._district_name, self._house_name, write_params
            return

        # if ParamType._P in write_params.keys() and ParamType._Q in write_params.keys():
        #     p = write_params[ParamType._P]
        #     q = write_params[ParamType._Q]
        #     if (ParamType._P not in self._old_write.keys() or (
        #             ParamType._P in self._old_write.keys() and self._old_write[ParamType._P] is not p)) or \
        #         (ParamType._Q not in self._old_write.keys() or (
        #             ParamType._Q in self._old_write.keys() and self._old_write[ParamType._Q] is not q)):
        #         pass


        for write_param, data in write_params.items():
            if write_param in self._acs800_register.keys():
                if (write_param not in self._old_write.keys()) or (
                                write_param in self._old_write.keys() and self._old_write[write_param] is not data):
                    if self._con['acs800'].write_multiple_registers(self._acs800_register[write_param],
                                                                    [data, 0, 0]) is None:
                        print 'grilabhouse, cannot write register'
                    self._old_write[write_param] = data

    def physical_read_params(self):
        """
        physical_read_params(self)

        Read params from the system using modbus over TCP.
        In this case all the data can be read in one time

        :return: read value
        """
        if DEGUC_COM:
            list = []
            index = self._house_name * 25
            for i in self.rlist:
                list.append(random.uniform(-10000, 10000))
                index -= 1
            self.update_plot_measure(list[:])
            return list

        res = []
        for sicam in self._sicam_register:
            data = self._con['sicam'].read_holding_registers(self._slave, sicam['all'],
                                                             sicam['quantity'] * 2)
            if data is None:
                res.append(None * sicam['quantity'])
                continue
            data = data.encode('hex')
            #print 'phyiscal_read_params', len(data)
            # if len(data) != sicam['quantity'] * 8 + 6 + 4: #316,180
            #     res.append(None * sicam['quantity'])
            #     continue

            # todo check for crc with the last two bytes

            for i in range(sicam['quantity']):
                d = data[6 + i * 8:6 + (i + 1) * 8]  # get data frame
                res.append(ModBusOverTcp.measured_value_float_decode(d))

        self.update_plot_measure(res[:])

        return res

    ####################################################################################################################
    ## Converter ??
    ####################################################################################################################

    def physical_converter_params(self, write_params):
        if self.converters is None:
            return write_params

        wp = {}
        if ParamType._P in write_params.keys() and ParamType._Q in write_params.keys():
            p = write_params[ParamType._P]
            q = write_params[ParamType._Q]

            if (p >= 0 and q >= 0) or (p <= 0 and q <= 0):
                if ConverterType._first_third_quadrant_pq in self.converters.keys():
                    wp = self.converters[ConverterType._first_third_quadrant_pq].call(write_params)
                else:
                    raise Exception('No converter object provided')
            else:
                if ConverterType._second_fourth_quadrant_pq in self.converters.keys():
                    wp = self.converters[ConverterType._second_fourth_quadrant_pq].call(write_params)
                else:
                    raise Exception('No converter object provided')

        return wp

    ####################################################################################################################
    ## Security
    ####################################################################################################################

    def priori_security_check(self):
        # return self.write_physical
        return {}

    def posteriori_security_check(self):
        # read current I1,I2,I3
        return {}

    def fast_correction(self, write_param):
        if DEGUC_COM:
            print write_param, self._house_name
            return

        print write_param
        self.physical_write_params(self.physical_converter_params(write_param))

    ####################################################################################################################
    ## Regulation
    ####################################################################################################################

    def init_regulator(self):
        # print 'init_regulator', self.friendly_name, self.write_physical
        for k, v in self.write_physical.items():
            if k in self._pid.keys():
                # print 'new set point', v
                self._pid[k].clear()
                self._pid[k].set_point(v)

    def physical_read_regulation(self, write_param):
        if DEGUC_COM:
            return random.uniform(0, 1000)

        if write_param not in self._single_write_register.keys():
            return

        data = self._con['sicam'].read_holding_registers(self._slave,
                                                         self._single_write_register[write_param], 2).encode('hex')

        return ModBusOverTcp.measured_value_float_decode(data[6:6 + 8])

    def physical_regulation_params(self, write_params):
        # TODO Check the len of write_params otherwise data maybe lost
        for k, v in write_params.items():  # get the data
            if k in self._pid.keys():
                return self._pid[k].update(v)

    def end_regulator(self):
        pass
        # print 'end_regulator', self.friendly_name, self.write_physical

    ####################################################################################################################
    ## TODO Modbus Read-Write
    ####################################################################################################################

    def modbus_read(self, offset, length=2):
        if DEGUC_COM:
            data = [random.uniform(0, 100) for i in range(length)]
            return data

        data = self._con['sicam'].read_holding_registers(self._slave, offset, length).encode('hex')
        return ModBusOverTcp.measured_value_float_decode(data[6:6 + 8])

    def modbus_write(self, offset, data):
        if (offset not in self._old_write.keys()) or (
                        data in self._old_write.keys() and self._old_write[offset] is not data):
            # TODO check for write fail
            self._con['acs800'].write_multiple_registers(offset, [data])
            # update old write, it's useful for not writing the same value all the time
            self._old_write[offset] = data
