"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridsim.cyberphysical import CyberPhysicalModule
from gridsim.cyberphysical.simulation import CyberPhysicalGroup
from gridsim.cyberphysical.aggregator import SumAggregator
from gridsim.cyberphysical.element import Actor
from gridsim.decorators import accepts, returns

from gridlab.gridlabcyberphysicalsystem.feedermeter import FeederMeter
from gridlab.gridlabcyberphysicalsystem.gridlabhouse import GridLabHouse
from gridlab.gridlabcyberphysicalsystem.svc import SVC

# TODO change the following line
from gridlab.paramtype import ConverterType, ParamType
from gridlab.regulation.converter import SecondFourthQuadrantConverter, FirstThirdQuadrantConverter

from gridsim.cyberphysical.regulation import RegulationManager, RegulatorGroup
from gridsim.cyberphysical.security import SecurityManager, SecurityGroup
from gridlab.security.current import CurrentSecurity

from district import existing_district_name

from gridplot.connector.plotconnector import PlotListener


class Feeder(object):
    def __init__(self, district_name):
        super(Feeder, self).__init__()

        self.district_name = district_name

        self.gridlab_house = {}
        self.feeder_meter = None
        self.svc = None

        self._security_group = None
        self._security_rules = None
        # self._security_manager = None
        self._sim_group = None
        self._regulation_group = None

    @returns(bool)
    def initialize_district(self):

        if self.district_name not in existing_district_name:
            return False

        # declare converter for the basic district control
        converter = {ConverterType._first_third_quadrant_pq: FirstThirdQuadrantConverter(),
                     ConverterType._second_fourth_quadrant_pq: SecondFourthQuadrantConverter()}
        # declare write params for houses
        writegridlab = [(ParamType._P, SumAggregator(-15000, 15000)),
                        (ParamType._Q, SumAggregator(-15000, 15000))]

        self._sim_group = CyberPhysicalGroup()
        self._regulation_group = RegulatorGroup()
        self._regulation_group.name = str(self.district_name)

        # create houses
        for i in range(3):
            gridlabhouse = GridLabHouse("gridlab_house_" + str(i + 1) + self.district_name, converter)
            gridlabhouse.initialize(self.district_name, i + 1, write_params=writegridlab)
            self.gridlab_house["gridlab_house_" + str(i + 1) + self.district_name] = gridlabhouse
            self._regulation_group.add_regulator(gridlabhouse)
            self._sim_group.acps.append(gridlabhouse)

        # create feedermeter
        self.feeder_meter = FeederMeter("gridlab_feeder_" + self.district_name)
        self.feeder_meter.initialize(self.district_name)
        self._sim_group.acps.append(self.feeder_meter)

        # create svc
        self.svc = SVC("gridlab_svc_" + self.district_name)
        # self.svc.initialize()
        self._sim_group.acps.append(self.svc)

        return True

    @accepts((1, CyberPhysicalModule))
    def register_district(self, cyber_physical_module):
        for house in self.gridlab_house.values():
            cyber_physical_module.add_cps_listener(house)
            cyber_physical_module.add_module_listener(house)
        cyber_physical_module.add_cps_listener(self.feeder_meter)
        cyber_physical_module.add_cps_listener(self.svc)
        cyber_physical_module.add_acps_group_listener(self._sim_group)

    @accepts((1, PlotListener))
    def register_plot(self, plot):
        for house in self.gridlab_house.values():
            house.add_plot(plot)

    @accepts((1, Actor))
    def register_actor(self, actor):
        for house in self.gridlab_house.values():
            house.add(actor)
        self.feeder_meter.add(actor)
        self.svc.add(actor)

    @accepts((1, RegulationManager))
    def register_regulation(self, regulation):
        # for house in self.gridlab_house.values():
        #     regulation.add_regulator(house)
        regulation.add_regulator_group(self._regulation_group)

    @accepts((1, SecurityManager))
    def register_security(self, security):
        self._security_group = SecurityGroup()
        self._security_rules = CurrentSecurity()
        for house in self.gridlab_house.values():
            self._security_group.add_security(house)
        self._security_group.add_security(self.feeder_meter)

        self._security_group.set_rules(self._security_rules)

        security.add_posteriori_security_group(self._security_group)
