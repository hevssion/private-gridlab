"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridsim.cyberphysical.element import AbstractCyberPhysicalSystem
from ctypes import *

from enum import Enum

from gridsim.decorators import accepts, returns


class SvcRegister(Enum):
    consign_u_out_rms = 5

    temperature = 33
    u_grid_rms = 34
    u_out_1_rms = 35
    u_out_3_rms = 37
    i_out_1_rms = 39
    i_out_3_rms = 41
    p_out_1 = 43
    p_out_3 = 45
    s_out_1 = 47
    s_out_3 = 49
    pf_out_1 = 51
    pf_out_3 = 53
    u_supply_flyback = 55
    freq_network = 56
    i_out_1_rms_smooth = 58
    i_out_1_max = 59
    i_out_3_rms_smooth = 60
    i_out_3_max = 61
    rms_integrated_channel_1 = 62
    rms_integrated_channel_3 = 64


class SVC(AbstractCyberPhysicalSystem):
    def __init__(self, friendly_name):
        super(SVC, self).__init__(friendly_name, None)

        self._host = None
        self._port = None

        self._dspctrl = None

        # c_type
        self._dspctrl_connection = [None, None, None]

    def initialize(self):
        dll_path = 'libDspCtrl.dll'
        self._dspctrl = WinDLL(dll_path)

        # function defined
        # connection function
        self._dspctrol_connect_function = self._dspctrl['cDspCtrlConnect']
        self._dspctrol_connect_function.restype = c_void_p
        self._dspctrol_connect_function.argtypes = (c_char_p, c_char_p, c_char_p)

        # isready function
        self._dspctrl_isready_function = self._dspctrl["cDspCtrlIsReady"]
        self._dspctrl_isready_function.restype = c_int32
        self._dspctrl_isready_function.argtypes = (c_void_p,)

        # readparam function
        self._dspctrol_readparam_function = self._dspctrl['cDspCtrlReadParameter']
        self._dspctrol_readparam_function.restype = c_int32
        self._dspctrol_readparam_function.argtypes = (c_void_p, c_uint8, c_int, c_uint8, POINTER(c_double))

        # writeparam function
        self._dspctrol_writeparam_function = self._dspctrl['cDspCtrlWriteParameter']
        self._dspctrol_writeparam_function.restype = c_int32
        self._dspctrol_writeparam_function.argtypes = (c_void_p, c_uint8, c_int, c_uint8, c_double)

    def dspctrl_connect(self, host):
        for i in range(0, 3):
            connection = self._dspctrol_connect_function("tms320x28xx", "tcp", host + ':' + str(100 + i))
            if connection == 0:
                raise Exception('DspCtrl cannot connect to host!')
            self._dspctrl_connection[i] = connection

    def dspctrl_disconnect(self):
        pass

    def dspctrl_isready(self, connection):
        res = self._dspctrl_isready_function(self._dspctrl_connection[connection])
        if res == -1:
            raise Exception('DspCtrl read ready to host fail!')
        return res

    @accepts(((1, 2, 3, 4), (int)))
    #@returns((1,int),(2,float))
    def dspctrl_read_parameter(self, connection, parameter_id, type, fpp):
        value = c_double(0)
        res = self._dspctrol_readparam_function(self._dspctrl_connection[connection], parameter_id, type, fpp,
                                                byref(value))
        if res == -1:
            print 'DspCtrl cannot read parameter from host!'
        return res,value.value

    @accepts(((1, 2, 3, 4), (int)), (5, (int,float)))
    @returns(int)
    def dspctrl_write_parameter(self, connection, parameter_id, type, fpp, value):
        res = self._dspctrol_writeparam_function(self._dspctrl_connection[connection], parameter_id, type, fpp, value)
        if res == -1:
            print 'DspCtrl cannot write parameter to host!'
        return res

    def physical_read_params(self):
        pass

    def physical_write_params(self, write_params):
        pass

    def init_regulator(self):
        pass

    def physical_read_regulation(self, write_param):
        pass

    def physical_regulation_params(self, write_params):
        pass

    def end_regulator(self):
        pass
