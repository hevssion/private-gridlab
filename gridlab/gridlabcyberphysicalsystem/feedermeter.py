"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""
from gridsim.cyberphysical.core import ReadParam
from gridsim.cyberphysical.element import AbstractCyberPhysicalSystem, Actor
from gridsim.decorators import accepts

from gridlab.gridlabcyberphysicalsystem.district import DisctrictInitializer, DEGUC_COM
from gridplot.connector.plotconnector import AbstractPlotSystem, PlotParam
from gridsim.cyberphysical.security import Security

import random

from gridlab.modbus.modbus import ModBusOverTcp

from gridlab.paramtype import ParamType


class FeederMeter(AbstractCyberPhysicalSystem, AbstractPlotSystem,Security):
    def __init__(self, friendly_name):
        """
        __init__(self,friendly_name)

        This class communicate using modbus protocol to the asc800 drives and sicam meters

        :param friendly_name: name given to the gridsim Element
        """
        AbstractCyberPhysicalSystem.__init__(self,friendly_name,None)
        AbstractPlotSystem.__init__(self)
        Security.__init__(self)

        # ReadParam list supported
        self.rlist = None
        # WriteParam list supported
        self.wlist = None

        self._district_name = ''
        self._house_name = 5

        # Technical information to exchange data on modbus
        self._sicam_register = []

        # Id of the district
        self._slave = 0
        self._con = None

    def initialize(self, district_name, read_params=None, sicam_register=None, slave=None):
        """
        initialize(self, readparamlist, sicamregister, writeparamlist, acs800register, slave)

        Initialize the ReadParam and the WriteParam

        :param district_name: the district letter
        :param read_params: readparam list supported
        :param sicam_register: sicam modbus technical information
        :param slave:
        """
        self._district_name = district_name

        self._con = {}
        if not DEGUC_COM:
            self._con = DisctrictInitializer.initialize_host(district_name)

        self._slave = slave
        self.rlist = read_params
        self._sicam_register = sicam_register
        self._house_number = 5

        # get the default value if not provided for slave, sicamregister and readparamlist
        if slave is None or read_params is None or sicam_register is None:
            self._slave, self._sicam_register, self.rlist = DisctrictInitializer.initialize_read_register_param(
                district_name, self._house_number,9)

        # check the number of read params list
        # if 'quantity' not in self._sicam_register or self._sicam_register['quantity'] is not len(self.rlist):
        #     raise Exception('ReadParam does not match the registers value')

        # create readparam list for the cyberphysical system
        for r in self.rlist:
            self.read_params.append(ReadParam(r, (r, district_name, self._house_number)))

    @accepts((1, Actor))
    def add(self, actor):
        rparamlist = actor.read_params
        wparamlist = actor.write_params

        # TODO change loop order
        # register write actors
        for w in self.write_params:
            for a in wparamlist:
                if a == w.info:
                    w.add_callable(actor)
                elif len(a) is 1 and a[0] is w.write_param:
                    w.add_callable(actor)
        # register read actors
        for r in self.read_params:
            for a in rparamlist:
                if a is r.info:
                    r.add_listener(actor)
                elif len(a) is 1 and a[0] is r.read_param:
                    r.add_listener(actor)

        actor.id = len(self.actors)
        self.actors.append(actor)
        return actor

    ####################################################################################################################
    ## Physical Device
    ####################################################################################################################

    # no regulation, because no write param
    def physical_write_params(self, write_params):
        pass

    def physical_read_params(self):
        """
        physical_read_params(self)

        Read params from the system using modbus over TCP.
        In this case all the data can be read in one time

        :return: read value
        """
        if DEGUC_COM:
            list = []
            for i in self.rlist:
                list.append(random.uniform(0, 1000))
            return list
        res = []
        for sicam in self._sicam_register:
            data = self._con['sicam'].read_holding_registers(self._slave, sicam['all'],
                                                             sicam['quantity'] * 2).encode('hex')


            for i in range(sicam['quantity']):
                d = data[6 + i * 8:6 + (i + 1) * 8]  # get data frame
                res.append(ModBusOverTcp.measured_value_float_decode(d))

        #self.update_plot_measure(res[:])

        return res

    ####################################################################################################################
    ## Security
    ####################################################################################################################

    # no security, because no write param
    def priori_security_check(self):
        return {}

    def posteriori_security_check(self):
        # read current I5
        if DEGUC_COM:
            return {ParamType._ISUM:random.uniform(0, 80)}
        data = self._con['sicam'].read_holding_registers(self._slave, 40225, 2).encode('hex')
        isum = ModBusOverTcp.measured_value_float_decode(data[6:6 + 8])

        return {(ParamType._ISUM,self._district_name,5):isum}

    # no security, because no write param
    def fast_correction(self, write_param):
        pass

    ####################################################################################################################
    ## Regulation
    ####################################################################################################################

    def init_regulator(self):
        pass

    def physical_read_regulation(self, write_param):
        pass

    def physical_regulation_params(self, write_params):
        pass

    def end_regulator(self):
        pass
