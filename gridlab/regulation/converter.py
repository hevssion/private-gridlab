"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

import struct

from gridlab.gridlabcyberphysicalsystem.gridlabhouse import ParamType
from gridsim.cyberphysical.core import Converter


class SecondFourthQuadrantConverter(Converter):
    def __init__(self):
        """
        __init__(self)

        Convert the data to a writable value for the simulation
        """
        super(SecondFourthQuadrantConverter, self).__init__()

    def call(self, datas):
        """
        call(self,datas)

        Convert reactive power

        :param datas: data to convert
        :return: the converted value
        """
        res = {}
        for paramtype, value in datas.items():
            data = 0
            if ParamType._P is paramtype:
                data = value * 10.16 / 22
            elif ParamType._Q is paramtype:
                data = (value - 2050) / -2.36
            res[paramtype] = int(struct.pack('>h', data).encode('hex'), 16)

        return res


class FirstThirdQuadrantConverter(Converter):
    def __init__(self):
        """
        __init__(self)

        Convert the data to a writable value for the simulation
        """
        super(FirstThirdQuadrantConverter, self).__init__()

    def call(self, datas):
        """
        call(self,datas)

        Convert active power

        :param datas, data to convert
        :return the converted value
        """
        res = {}
        for paramtype, value in datas.items():
            data = 0
            if ParamType._P is paramtype:
                data = value * 10 / 22
            elif ParamType._Q is paramtype:
                data = (value - 1990) / -2.254
            res[paramtype] = int(struct.pack('>h', data).encode('hex'), 16)

        return res
