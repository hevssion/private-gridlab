
class PIDRegulator(object):
    def __init__(self, kp, ki, kd, dt = 1):
        super(PIDRegulator, self).__init__()

        self._kp = kp
        self._ki = ki
        self._kd = kd

        self._dt = dt

        self._set_point = 0.0
        self._last_error = 0.0

        self._ITerm = 0.0

        self.output = 0.0

    def set_point(self, set_point):
        self._set_point = set_point

    def clear(self):
        self._set_point = 0.0
        self._last_error = 0.0
        self._ITerm = 0.0

        self.output = 0.0

    def update(self, feedback):
        error = self._set_point - feedback

        if error > 5000:
            return 0

        delta_error = error - self._last_error

        PTerm = self._kp * error
        self._ITerm = (self._ITerm + error* self._dt) * self._ki
        DTerm = self._kd * delta_error / self._dt

        self._last_error = error

        self.output = PTerm + self._ITerm + DTerm
        return  self.output

if __name__ == '__main__':
    pid = PIDRegulator(0.5,0.3,0.2)

    pid.clear()
    pid.set_point(3000)
    print pid.update(2924) + 3000
    print pid.update(2998) + 3000
    print pid.update(2992) + 3000
