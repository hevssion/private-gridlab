"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridlab.gridlabcyberphysicalsystem.district import bcolors
from gridlab.paramtype import ParamType
from gridsim.cyberphysical.security import SecurityRules

from scipy import *


class CurrentSecurity(SecurityRules):
    def __init__(self):
        super(CurrentSecurity, self).__init__()

        self._limit_current = 40

    def initialize_current_rules(self, limit_current=40):
        self._limit_current = limit_current

    def check_priori_rules(self, param):
        for it in param:
            # trust couple, organization
            if ParamType._P in it.keys() and ParamType._Q in it.keys():
                p = it.values()[0]
                q = it.values()[1]

                s = sqrt(pow(p, 2) + pow(q, 2))
                current = s / 230

                if current > self._limit_current:
                    print bcolors.WARNING + 'Warning limit current will be to high' + bcolors.ENDC
                    return {ParamType._P: 0, ParamType._Q: 0}

            # TODO or check for correct couple using key

    def check_posteriori_rules(self, param):
        # print 'check, rules',param
        for k, v in param.items():
            if v > self._limit_current:
                print bcolors.WARNING + 'Warning limit current exceeded' + bcolors.ENDC
                return {ParamType._P: 0, ParamType._Q: 0}
