"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

import socket

import struct
from pyModbusTCP.utils import crc16

import time

class ModBusOverTcp(object):
    def __init__(self, host, port=502):
        """
        __init__(self, address, port) test

        Initialize the ModbusOverTcp client to connect to host. Request a 32-bit float data type
        in output (holding) register (on send function).

        :param address: address of the modbus host
        :param port: port of th modbus host default is 502
        """
        super(ModBusOverTcp, self).__init__()

        self._host = host
        self._port = port

        self.s = None

    def open(self):
        """
        open(self)

        Open the given communication and wait for a connection state

        :return: True on connection sucess, Flase otherwise
        """
        #if self.is_open():
        #    self.close()

        for res in socket.getaddrinfo(self._host, self._port,
                                      socket.AF_UNSPEC, socket.SOCK_STREAM):

            af, sock_type, proto, canon_name, sa = res
            try:
                self.s = socket.socket(af, sock_type, proto)
            except socket.error:
                self.s = None
                continue
            try:
                self.s.settimeout(30.0)
                self.s.connect(sa)
                self.s.setblocking(0)
            except socket.error:
                self.s.close()
                self.s = None
                continue
            break
        if self.s is not None:
            return True
        else:
            return False

    def send_raw(self,raw_message):
        """
        send_raw(self, raw_message)

        Send a raw message over the network to the host

        :param raw_message: raw message to send
        :return: total of bytes sent
        """
        totalsent = 0
        sent = self.s.send(raw_message)
        if sent == 0:
            raise RuntimeError("socket connection broken")
        totalsent = totalsent + sent
        return totalsent

    def read_holding_registers(self, slave_address, register, quantity):
        """
        send(self, slave_address, register, quantity)

        Send a formatted message to the host

        :param slave_address: specify the slave_address target
        :param register: the register number to read or write
        :param quantity: the quantity (len) of register to read starting at the register param value
        :return: total of byte sent
        """

        f_body = struct.pack('B', 0x03) + struct.pack('>HH', register, quantity)

        slave_ad = struct.pack('B', slave_address)
        crc = struct.pack('<H', crc16(slave_ad + f_body))
        tx_buffer = slave_ad + f_body + crc

        self.s.send(tx_buffer)

        # time.sleep(0.5)
        #
        # try:
        #     data = self.s.recv(1024)
        #     return data
        #
        # except socket.timeout as err:
        #     print err
        # except socket.error as err:
        #     print err
        #
        # #TODO check data before return
        # return None

        timeout = 2
        total_data = []
        len_data = 0
        begin = time.time()

        total_len = quantity*2+6

        while 1:
            if len_data == total_len:
                break
            elif time.time() - begin > timeout * 2:
                break
            try:
                data = self.s.recv(1024)
                len_data += len(data)
                total_data.append(data)
            except:
                pass
        return ''.join(total_data)

    @staticmethod
    def measured_value_float_decode(val):
        """
        measured_value_float_decode(self,val)

        Decode the modbus message to retrieve the value from the register.

        :param val: message to decode
        :return: message decoded (32 bits float format)
        """
        bframe = bin(int(val, 16))
        bframe = bframe[2:]
        bframe = bframe.rjust(32, '0')

        mantissa = bframe[len(bframe) - 23:]  # precision
        exp = int('0b' + bframe[len(bframe) - 23 - 8:9], 2)  # offset
        s = int('0b' + bframe[:1], 2)  # bit sign

        vmantissa = 0.

        for n,i in enumerate(str(mantissa),1):
            vmantissa = vmantissa + float(i)/(pow(2,n))

        res = pow(-1, s) + 0.
        res = res * pow(2, exp - 127)
        res = res * (1.0 + vmantissa)

        return res
