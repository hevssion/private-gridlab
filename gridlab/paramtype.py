"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from enum import Enum

class ParamType(Enum):
    """
    ParamType list using in this program to identify the values
    """

    __order__ = '_U1 _U2 _U3 _UN _I1 _I2 _I3 _IN _UL12 _UL23 _UL31 _USUM _ISUM _PL1 _PL2 _PL3 _PA _QL1 _QL2 _QL3 _QA _SL1 _SL2 _SL3 _SA _P _Q'

    # unit 1
    _U1 = 0  # readable value
    _U2 = 1
    _U3 = 2
    _UN = 3
    _I1 = 4
    _I2 = 5
    _I3 = 6
    _IN = 7
    _UL12 = 8
    _UL23 = 9
    _UL31 = 10
    _USUM = 11
    _ISUM = 12
    _PL1 = 13
    _PL2 = 14
    _PL3 = 15
    _PA = 16  # 40233
    _QL1 = 17
    _QL2 = 18
    _QL3 = 19
    _QA = 20  # 40241
    _SL1 = 21
    _SL2 = 22
    _SL3 = 23
    _SA = 24
    _COS_PHI_L1 = 25
    _COS_PHI_L2 = 26
    _COS_PHI_L3 = 27
    _COS_PHI = 28
    _PF_L1 = 29
    _PF_L2 = 30
    _PF_L3 = 31
    _PF = 32
    _PHI_L1 = 33
    _PHI_L2 = 34
    _PHI_L3 = 35
    _PHI_SUM = 36
    _f = 37

    _WpEd = 606
    _WpL1s = 607
    _WpL2s = 608
    _WpL3s = 609
    _WpEs = 610
    _WpL1t = 611
    _WpL2t = 612
    _WpL3t = 613
    _WpEt = 614
    _WqL1t = 615
    _WqL2t = 616
    _WqL3t = 617
    _WqEt = 618
    _WqL1i = 619
    _WqL2i = 620
    _WqL3i = 621
    _WqEi = 622
    _WqL1c = 623
    _WqL2c = 624
    _WqL3c = 625
    _WqEc = 626

    _P = 1000  # writable value
    _Q = 1001  #

    @staticmethod
    def get_quantity(group=0):
        """
        getQuantity()

        return the number of ReadParam per district

        :return: number of ReadParam per district
        """
        if group == 0:
            return 38
        elif group == 8:
            return 21

    @staticmethod
    def get_offset(group=0):
        """
        get_offset(group)

        return the group offset

        :param group: group to return the offset
        :return: return the group offset
        """
        if group == 0:
            return 0
        elif group == 8:
            return 606

class SetPointType(Enum):

    __order__ = '_Cos_Phy_1 _Cos_Phy_2 _Cos_Phy_3 _simple_test1 _simple_test2'

    _Cos_Phy_1 = 10000
    _Cos_Phy_2 = 10001
    _Cos_Phy_3 = 10002

    _simple_test1 = 10003
    _simple_test2 = 10004


class ConverterType(Enum):
    """
    ParamType list using in this program to identify the value
    """

    # unit 1
    _first_third_quadrant_pq = 0  # readable value
    _second_fourth_quadrant_pq = 1
