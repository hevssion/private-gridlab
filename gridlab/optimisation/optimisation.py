"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridlab.actor.genericcallable import GenericCallable

from solution_optim_district import fct_cout_optim_district

from gridlab.paramtype import ParamType, SetPointType

from scipy import *
from scipy.optimize import differential_evolution


class VoltageOptimisation(GenericCallable):
    def __init__(self, district=None):
        super(VoltageOptimisation, self).__init__(district)

        self._debug_phy = [0, 0, 0]

    def init_callable(self):
        global P_inv_A
        global P_inv_B
        global P_inv_C

        P_inv_A = 0.0
        P_inv_B = 0.0
        P_inv_C = 0.0

        self.read_params = [(ParamType._U1,), (ParamType._U2,), (ParamType._U3,), (ParamType._UN,),
                            (ParamType._I1,), (ParamType._I2,), (ParamType._I3,), (ParamType._IN,),
                            (ParamType._UL12,), (ParamType._UL23,), (ParamType._UL31,), (ParamType._USUM,),
                            (ParamType._ISUM,),
                            (ParamType._PL1,), (ParamType._PL2,), (ParamType._PL3,), (ParamType._PA,),
                            (ParamType._QL1,), (ParamType._QL2,), (ParamType._QL3,), (ParamType._QA,),
                            (ParamType._SL1,), (ParamType._SL2,), (ParamType._SL3,), (ParamType._SA,)]

        self.write_params = [(ParamType._Q, self.district, 1),
                             (ParamType._Q, self.district, 2),
                             (ParamType._Q, self.district, 3),
                             (SetPointType._Cos_Phy_1, self.district, 1),
                             (SetPointType._Cos_Phy_2, self.district, 2),
                             (SetPointType._Cos_Phy_3, self.district, 3)]

    def send_request_data(self, step, setpoint, measure):
        global P_inv_A
        global P_inv_B
        global P_inv_C
        global P_charge_A
        global P_charge_B
        global P_charge_C
        global P_charge_D
        global V_grid

        print 'setpoint', setpoint
        print 'measure', measure
        print 'step', step

        # raise Exception('')

        # without optimisation
        if step > 15:
            return {(ParamType._Q, self.district, 1): 0,
                    (ParamType._Q, self.district, 2): 0,
                    (ParamType._Q, self.district, 3): 0}

        P_charge_A = 0.
        P_charge_B = 0.
        P_charge_C = 0.

        # with optimisation
        if len(setpoint.keys()) == 6:  # inv and load
            P_inv_A = setpoint.values()[0]
            P_inv_B = setpoint.values()[1]
            P_inv_C = setpoint.values()[2]
            P_charge_A = setpoint.values()[3]
            P_charge_B = setpoint.values()[4]
            P_charge_C = setpoint.values()[5]
        elif len(setpoint.keys()) == 3:  # only inv
            P_inv_A = setpoint.values()[0]
            P_inv_B = setpoint.values()[1]
            P_inv_C = setpoint.values()[2]

        #if len(measure.values()) == 100:
        V_grid = measure.values()[75+11]
        print 'ugrid',V_grid

        # inputs a determiner:
        cos_phi_max = 0.3
        phi_init = [0, 0, 0]
        # phi_init= [arccos(0.99), arccos(0.99), arccos(0.99)]
        phi_MAX = arccos(cos_phi_max)
        phi_MIN = -arccos(cos_phi_max)
        phi_MAX_list = [arccos(cos_phi_max), arccos(cos_phi_max), arccos(cos_phi_max)]
        phi_MIN_list = [-arccos(cos_phi_max), -arccos(cos_phi_max), -arccos(cos_phi_max)]

        # f_cout_init = fct_cout_optim_district(phi_init)

        bounds = [(phi_MIN, phi_MAX), (phi_MIN, phi_MAX), (phi_MIN, phi_MAX)]
        result = differential_evolution(fct_cout_optim_district, bounds, strategy='best1bin')

        self._debug_phy[0] = result.x[0]
        self._debug_phy[1] = result.x[1]
        self._debug_phy[2] = result.x[2]

        q_inv_a = -P_inv_A * tan(result.x[0])
        q_inv_b = -P_inv_B * tan(result.x[1])
        q_inv_c = -P_inv_C * tan(result.x[2])

        # print 'phi',result.x[0],result.x[1],result.x[2]
        print 'p:',P_inv_A,P_inv_B,P_inv_C
        print 'q:',q_inv_a,q_inv_b,q_inv_c

        return {(ParamType._Q, self.district, 1): q_inv_a,
                (ParamType._Q, self.district, 2): q_inv_b,
                (ParamType._Q, self.district, 3): q_inv_c}

    def debug_request_data(self):
        # return debug array filled to provide information about the debug
        return {(SetPointType._Cos_Phy_1, self.district, 1): self._debug_phy[0],
                (SetPointType._Cos_Phy_2, self.district, 2): self._debug_phy[1],
                (SetPointType._Cos_Phy_3, self.district, 3): self._debug_phy[2]}

    def get_name(self):
        return 'pqoptimisation' + self.district
