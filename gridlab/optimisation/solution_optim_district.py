# solution_optim_district.py
# Moix P-O
# jan 2017 v1


# import scipy
from scipy import *
from scipy.optimize import differential_evolution
import matplotlib.pyplot as plt
import numpy as np

# ma fonction de simulation du district:
from fct_sim_district import fct_sim_district


# import global_variable_and_constants


# class
#
#     self._
#
# def ii()

def fct_cout_optim_district(phi_inv):
    # fct_cout_optim_district.m
    # Moix P-O dec 2016 v1
    #
    # En entree les 3 cos phi  en rad qu l'on peut controler comme un array de numpy:
    # En sortie
    # la fonction cout: f_cout
    #

    # donnees de global pour la simulation (pas passe en argument car les
    # entrees de la fonction ne sont que les variable de l'espace des param utilise pour l optimisation

    global P_inv_A
    global P_inv_B
    global P_inv_C  # la puissance solaire est mesuree
    global P_charge_A
    global P_charge_B
    global P_charge_C
    global P_charge_D  # la puissance consommateur est mesuree par un smartmeter
    global phi_charge_A
    global phi_charge_B
    global phi_charge_C
    global phi_charge_D
    global V_grid  # [V] au transfo
    global I_want_plot

    # print("P_inv_A tri= ", P_inv_A)

    Pinv = [P_inv_A, P_inv_B, P_inv_C]  # en [W]
    #  phi_inv=[phi_inv_A, phi_inv_B, phi_inv_C]
    phi_inv_list = [0, 0, 0]
    phi_inv_list[0] = phi_inv[0]  # en [rad]
    phi_inv_list[1] = phi_inv[1]
    phi_inv_list[2] = phi_inv[2]

    P_charge = [P_charge_A, P_charge_B, P_charge_C, P_charge_D]
    phi_charge = [phi_charge_A, phi_charge_B, phi_charge_C, phi_charge_D]

    inputs_sim_district = Pinv + phi_inv_list + P_charge + phi_charge + [V_grid]
    # print("inputs to sim district= ", inputs_sim_district)


    output_sim = fct_sim_district(inputs_sim_district)

    # #sorties:
    # outputs(1)=abs(U1_c)     #la tension au noeud 1
    # outputs(2)=abs(U2_c)     #la tension au noeud 2
    # outputs(3)=abs(U3_c)     #la tension au noeud 3
    # outputs(4)=abs(U4_c)     #la tension au noeud 4
    # outputs(5)=Q_trafo       # reactif au transfo
    # outputs(6)=abs(I_L1_c)   # courant dans la section 1
    # outputs(7)=abs(I_L2_c)   # courant dans la section 2
    # outputs(8)=abs(I_L3_c)   # courant dans la section 3
    # outputs(9)=abs(I_L4_c)   # courant dans la section 4




    # minimiser le reactif:
    f_cout = abs(output_sim[4])  # exemple pour minimiser l'echange de reactif avec le reseau...

    # minimiser ...:
    # f_cout=...  A FAIRE


    #########
    # Solution Exemple de fonction cout :

    # les grandeurs venant de la simulation qui vont etre reutilisee:
    voltage_noeud = np.array([V_grid, output_sim[0], output_sim[1], output_sim[2], output_sim[3]])
    reactif_renvoye_au_reseau = abs(output_sim[4])
    courants_section = np.array([output_sim[5], output_sim[6], output_sim[7], output_sim[8]])

    R_section = 150e-3

    pertes_totales_sur_lignes = sum(courants_section ** 2) * R_section  # [W]

    if I_want_plot == 1:
        fig3 = plt.figure(3)
        plt.clf()
        # plt.plot(Urms_iter, 'bo-')
        plt.plot(voltage_noeud, 'g+-')
        plt.xlabel('Distance')
        plt.ylabel('Volts')
        plt.title("tensions sur la ligne")
        ax = fig3.gca()
        ax.grid(True)
        plt.draw()

    ###############################################
    # Ponderations
    prix_kWh = 0.15  # CHF/kWh
    prix_KVArh = 0.05  # CHF/kVArh
    prix_du_volt_au_dessus_de_DACHCZ = 0.11  # CHF/V/noeud
    prix_du_volt_au_dessus_de_DACHCZ_quadra = 0.015  # CHF/V^2/noeud
    prix_du_volt_au_dessus_de_240 = 0.1  # CHF/V/noeud
    prix_du_volt_au_dessus_de_245 = 0.15  # CHF/V quadratique/noeud
    prix_du_volt_au_dessous_de_220 = 0.02  # CHF/V/noeud
    prix_du_volt_au_dessous_de_220_quadra = 0.004  # CHF/V^2/noeud
    prix_du_volt_au_dessous_de_210 = 0.15  # CHF/V/noeud

    ######
    # calcul des grandeurs:
    ecart_230_carre = sum((voltage_noeud - 230.0) ** 2)  # divergence par rapport a 230V

    ecart_au_dessus_de_DACHCZ_vect = ((sign(voltage_noeud - 230.0 * 1.03) + 1.0) / 2.0) * (voltage_noeud - 230.0 * 1.03)
    ecart_au_dessus_de_DACHCZ = sum(ecart_au_dessus_de_DACHCZ_vect)
    ecart_au_dessus_de_DACHCZ_carre = sum(ecart_au_dessus_de_DACHCZ_vect ** 2)

    ecart_au_dessus_de_240_vect = ((sign(voltage_noeud - 240.0) + 1.0) / 2.0) * (voltage_noeud - 240.0)
    ecart_au_dessus_de_240 = sum(ecart_au_dessus_de_240_vect)
    ecart_au_dessus_de_240_carre = sum(ecart_au_dessus_de_240_vect ** 2)

    ecart_au_dessus_de_245_vect = ((sign(voltage_noeud - 245.0) + 1.0) / 2.0) * (voltage_noeud - 245.0)
    ecart_au_dessus_de_245 = sum(ecart_au_dessus_de_245_vect)
    ecart_au_dessus_de_245_carre = sum(ecart_au_dessus_de_245_vect ** 2)

    ecart_au_dessous_de_220_vect = ((sign(220.0 - voltage_noeud) + 1.0) / 2.0) * (220.0 - voltage_noeud)
    ecart_au_dessous_de_220 = sum(ecart_au_dessous_de_220_vect)
    ecart_au_dessous_de_220_carre = sum(ecart_au_dessous_de_220_vect ** 2)

    ecart_au_dessous_de_210_vect = ((sign(210.0 - voltage_noeud) + 1.0) / 2.0) * (210.0 - voltage_noeud)
    ecart_au_dessous_de_210 = sum(ecart_au_dessous_de_210_vect)

    # Les prix a l'heure de fonctionnement:
    prix_pertes = pertes_totales_sur_lignes * prix_kWh / 1000.0
    prix_reactif = reactif_renvoye_au_reseau * prix_KVArh / 1000.0
    prix_tension_DACHCZ = ecart_au_dessus_de_DACHCZ * prix_du_volt_au_dessus_de_DACHCZ
    prix_tension_un_peu_haute = ecart_au_dessus_de_240 * prix_du_volt_au_dessus_de_240
    prix_tension_dangereuse = ecart_au_dessus_de_245 * prix_du_volt_au_dessus_de_245
    prix_tension_un_peu_basse = ecart_au_dessous_de_220 * prix_du_volt_au_dessous_de_220
    prix_tension_basse = ecart_au_dessous_de_210 * prix_du_volt_au_dessous_de_210

    ########
    # le total:
    f_cout = prix_pertes + prix_reactif + prix_tension_un_peu_haute + prix_tension_dangereuse + prix_tension_basse
    return f_cout


# solution_optim_district.py
# Moix P-O
# jan 2017 v1


global P_inv_A
global P_inv_B
global P_inv_C  # la puissance solaire est mesuree
global P_charge_A
global P_charge_B
global P_charge_C
global P_charge_D  # la puissance consommateur est mesuree par un smartmeter
global phi_charge_A
global phi_charge_B
global phi_charge_C
global phi_charge_D
global V_grid  # [V] au transfo
global I_want_plot

# print(" ")
# print(" ****** solution_optim_district ****** ")
#
# # global_variable_and_constants.init()
#
# #Mesure du reseau:
P_inv_A = 6000.0 * 3.0
P_inv_B = 5000.0 * 3.0
P_inv_C = 7000.0 * 3.0  # la puissance solaire est mesuree
P_charge_A = 0.001
P_charge_B = 0.001
P_charge_C = 0.001
P_charge_D = 0.001  # la puissance consommateur est mesuree par un smartmeter
phi_charge_A = arccos(0.97)
phi_charge_B = arccos(0.97)
phi_charge_C = arccos(0.97)
phi_charge_D = arccos(0.97)
V_grid = 230.0  # [V] au transfo

I_want_plot = 0

# # inputs a determiner:
# cos_phi_max = 0.8
# phi_init = [0, 0, 0]
# # phi_init= [arccos(0.99), arccos(0.99), arccos(0.99)]
# phi_MAX = arccos(cos_phi_max)
# phi_MIN = -arccos(cos_phi_max)
# phi_MAX_list = [arccos(cos_phi_max), arccos(cos_phi_max), arccos(cos_phi_max)]
# phi_MIN_list = [-arccos(cos_phi_max), -arccos(cos_phi_max), -arccos(cos_phi_max)]
#
# # ####
# # #fait un essai avec une entree de cos phi a 0
# f_cout_init = fct_cout_optim_district(phi_init)
#
# print(" resultat de la fonction cout avec facteur de charge 1: ", f_cout_init)
#
# #######
# # Pilotage optimise
# #####
#
#
#
#
# #####
#
# # differential_evolution(func, bounds, args=(), strategy='best1bin',
# #                             maxiter=1000, popsize=15, tol=0.01,
# #                             mutation=(0.5, 1), recombination=0.7, seed=None,
# #                             callback=None, disp=False, polish=True,
# #                             init='latinhypercube'):
#
# bounds = [(phi_MIN, phi_MAX), (phi_MIN, phi_MAX), (phi_MIN, phi_MAX)]
# result = differential_evolution(fct_cout_optim_district, bounds, strategy='best1bin')
# table_pilotage_opt = result.x
# f_cout_opt0 = result.fun
#
# print(" phi voulus par l'optimisation: ", table_pilotage_opt)
#
# # print(result)
#
# # #########################
# # # Algo fmincon de MATLAB
# # table_pilotage_opt=fmincon(@fct_cout_optim_district, ...
# #     x0, A, b, Aeq, beq, lb, ub, nonlcon, ...
# #     optimset(   'TolFun', 0.1e-7, ...
# #     'MaxFunEvals', 5000, ...
# #     'MaxIter', 2000, ...
# #     'TolX',1e-5, ...
# #     'Algorithm','active-set')    )
# # #    'Display','iter',...
# #  #   'PlotFcns',@optimplotfval, ...
# # # 'interior-point'
# # #'trust-region-reflective'
# # #'sqp'
# # #'active-set'
#
# #
# # #########################
# # #Algo genetique de MATLAB
# # opts = gaoptimset('PlotFcns',@gaplotbestf)
# # table_pilotage_opt2 = ga(@fct_cout_optim_district,...
# #     length(x0),...
# #     A,b,Aeq,beq,...
# #     lb,ub,...
# #     nonlcon,...
# #     gaoptimset(   'TolFun', 1e-5, ...
# #     'Generations', 200, ...
# #     'PopulationType', 'doubleVector')    )
# # #    'Display','iter' , ...
# # # , ...
# # #     'PlotFcns',@gaplotbestf) ...
#
# # table_pilotage_opt=phi_MAX_list
#
#
#
# ####
# # resultat avec choix de l'algorithme:
# f_cout_opt = fct_cout_optim_district(table_pilotage_opt)
#
# print(" resultat de la fonction cout avec facteur de charge optimise par pythonDE: : ", f_cout_opt, " avec phi: ",
#       table_pilotage_opt)
#
# # f_cout_opt2 = fct_cout_optim_district( table_pilotage_opt2 )
# # disp(['resultat de la fonction cout avec facteur de charge optimise ga: ' num2str(f_cout_opt2) ])
#
#
#
# if I_want_plot == 1:
#     plt.show()
