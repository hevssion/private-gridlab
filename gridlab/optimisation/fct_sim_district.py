# fct_sim_district.py
# meme script que dans Matlab mais en syntaxe python

import scipy
from scipy import *
import cmath
import matplotlib.pyplot as plt
import numpy as np


def fct_sim_district(inputs):
    I_want_plot_local = 0

    # fct_sim_district.m
    # Moix P-O
    ###
    # version
    # 2016 v1.0
    # jan 2017 v1.1
    ###
    #
    # --------------------
    # En entree la puissance total des 3 injections en triphase et le cos(phi) de
    # l'injecteur solaire:
    #   Pinv=inputs(1:3)
    #   phi_inv=inputs(4:6)
    # avec:
    #   Pinv=[P_inv_A, P_inv_B, P_inv_C]  en [W]
    #   phi_inv=[phi_inv_A, phi_inv_B, phi_inv_C] en [rad]
    #    note: A est en fin de ligne et C le plus proche du depart
    #
    # et pour les charges la puissance totale en triphase:
    #   P_charge=inputs(7:10)
    #   phi_charge=inputs(11:14)
    # avec:
    #   P_charge=[P_charge_A, P_charge_B, P_charge_C, P_charge_D]  en [W]
    #   phi_charge=[phi_charge_A, phi_charge_B, phi_charge_C, phi_charge_D] en [rad]
    #
    # --------------------
    # En sortie
    # la tension aux 4 noeuds
    #   outputs(1:4)=[U1rms_iter, U2rms_iter, U3rms_iter, U4rms_iter]
    # le reactif mesure au transfo
    #   output(5)=Q_trafo      # reactif au transfo
    # le courant dans chaque ligne
    #    output(6)=abs(I_L1_c)  # courant dans la section 1
    #    output(7)=abs(I_L2_c)  # courant dans la section 2
    #    output(8)=abs(I_L3_c)  # courant dans la section 3
    #    output(9)=abs(I_L4_c)  # courant dans la section 4
    #


    # Urms=[U1rms_iter, U2rms_iter, U3rms_iter, U4rms_iter]

    # Pinv=inputs(1:3)
    # phi_inv=inputs(4:6)
    # P_charge=inputs(7:10)
    # phi_charge=inputs(11:14)


    #################
    # Production PV ABB A par phase (!calcul sur 230V monophase symetrique)
    P_inv_A = inputs[0] / 3.0  # [W]
    P_inv_B = inputs[1] / 3.0  # [W]
    P_inv_C = inputs[2] / 3.0  # [W]

    phi_inv_A = inputs[3]  # [rad]
    phi_inv_B = inputs[4]  # [rad]
    phi_inv_C = inputs[5]  # [rad]

    P_charge_A = inputs[6] / 3.0  # [W]
    P_charge_B = inputs[7] / 3.0  # [W]
    P_charge_C = inputs[8] / 3.0  # [W]
    P_charge_D = inputs[9] / 3.0  # [W]

    phi_charge_A = inputs[10]  # [rad]
    phi_charge_B = inputs[11]  # [rad]
    phi_charge_C = inputs[12]  # [rad]
    phi_charge_D = inputs[13]  # [rad]

    V_grid = inputs[14]  # [V]

    #######################
    # Donnees du reseau::
    # V_grid=230.0 #[V]
    phase_grid = 0.0 * pi / 180.0
    f_grid = 50.0  # [Hz]
    omega = 2.0 * pi * f_grid  # [rad/sec]

    v_grid_c = V_grid * exp(1j * phase_grid)  # tension complexe

    ###############
    # Propriete d'une section de ligne:
    R_section = 150.0 * 10 ** -3.0
    L_section = 450.0 * 10 ** -6
    ZL_c = R_section + 1j * omega * L_section  # Impedance complexe

    #################
    # Production PV ABB A par phase (!calcul sur 230V monophase symetrique)
    # print("P_inv_A_L1= ", P_inv_A, " Watts")
    # P_inv_A=Pinv(1)/3.0 #[W]
    # phi_inv_A=phi_inv(1)# [rad]
    # phi_inv=acos(0.5)
    Q_inv_A = P_inv_A * tan(phi_inv_A)
    # avec convention de signe de la charge:
    S_inv_A_c = -P_inv_A + 1j * Q_inv_A

    #################
    # Production PV ABB B
    # P_inv_B=Pinv(2)/3.0 #[W]
    # phi_inv_B=phi_inv(2)# [rad]
    # phi_inv=acos(0.5)
    Q_inv_B = P_inv_B * tan(phi_inv_B)
    # avec convention de signe de la charge:
    S_inv_B_c = -P_inv_B + 1j * Q_inv_B

    #################
    # Production PV ABB C
    # P_inv_C=Pinv(3)/3.0 #[W]
    # phi_inv_C=phi_inv(3)# [rad]
    # phi_inv=acos(0.5)
    Q_inv_C = P_inv_C * tan(phi_inv_C)
    # avec convention de signe de la charge:
    S_inv_C_c = -P_inv_C + 1j * Q_inv_C

    ########
    # Consommateur_A:
    # P_charge_A=P_charge(1)/3.0 #[W]
    # phi_charge_A=phi_charge(1)  #[rad]
    Q_charge_A = P_charge_A * tan(phi_charge_A)
    S_charge_A_c = P_charge_A + 1j * Q_charge_A
    Z_charge_A_c = conj(230.0 * 230.0 / S_charge_A_c)  # equivalent de puissance a 230V

    ########
    # Consommateur_B:
    # P_charge_B=P_charge(2)/3.0#[W]
    # phi_charge_B=phi_charge(2)  #[rad]
    Q_charge_B = P_charge_B * tan(phi_charge_B)
    S_charge_B_c = P_charge_B + 1j * Q_charge_B
    Z_charge_B_c = conj(230.0 * 230.0 / S_charge_B_c)  # equivalent de puissance a 230V

    ########
    # Consommateur_C:
    # P_charge_C=P_charge(3)/3.0#[W]
    # phi_charge_C=phi_charge(3)  #[rad]
    Q_charge_C = P_charge_C * tan(phi_charge_C)
    S_charge_C_c = P_charge_C + 1j * Q_charge_C
    Z_charge_C_c = conj(230.0 * 230.0 / S_charge_C_c)  # equivalent de puissance a 230V

    ########
    # Consommateur_D:
    # P_charge_D=P_charge(4)/3.0#[W]
    # phi_charge_D=phi_charge(4)  #[rad]
    Q_charge_D = P_charge_D * tan(phi_charge_D)
    S_charge_D_c = P_charge_D + 1j * Q_charge_D
    Z_charge_D_c = conj(230.0 * 230.0 / S_charge_D_c)  # equivalent de puissance a 230V

    ################
    # Bilan des prosummers
    Sp4_c = S_inv_A_c + S_charge_A_c
    Sp3_c = S_inv_B_c + S_charge_B_c
    Sp2_c = S_inv_C_c + S_charge_C_c
    Sp1_c = S_charge_D_c

    ################
    # Bilan sans les pertes dans les lignes (provisoire pour premiere estim)
    S4_c = Sp4_c
    S3_c = Sp3_c + S4_c
    S2_c = Sp2_c + S3_c
    S1_c = Sp1_c + S2_c

    #################
    # premiere estimation du courant dans les lignes et de la chute de tension
    # dans chaque section en commencant par le debut de la ligne:
    U0_c = v_grid_c

    I_L1_c = conj(S1_c / U0_c)
    U_L1_c = I_L1_c * ZL_c  # chute sur la ligne de la section
    U1_c = U0_c - U_L1_c  # tension au bout de la section

    I_L2_c = conj(S2_c / U1_c)
    U_L2_c = I_L2_c * ZL_c  # chute sur la ligne de la section
    U2_c = U1_c - U_L2_c  # tension au bout de la section

    I_L3_c = conj(S3_c / U2_c)
    U_L3_c = I_L3_c * ZL_c  # chute sur la ligne de la section
    U3_c = U2_c - U_L3_c  # tension au bout de la section

    I_L4_c = conj(S4_c / U3_c)
    U_L4_c = I_L4_c * ZL_c  # chute sur la ligne de la section
    U4_c = U3_c - U_L4_c  # tension au bout de la section

    # Sur cette premiere estimation on peut faire des iterations pour affiner
    # le resultat

    # print(" " )
    # print("Z_charge_D_c= ", Z_charge_D_c )
    # print("S_2_c= ", S2_c )
    # print(" " )



    #############
    # Solver basique avec quelques iterations
    # pour etat a la fin de ligne (injection en phase a la fin de ligne)
    gamma = 0  # init nul
    beta_angle = 0  # init nul
    # gamma=phase_grid

    for loop in range(8):
        # reproduit le calcul mais en reprenant la bonne tension plutot que la
        # premiere estimation

        I_L1_c = conj(Sp1_c / U1_c) + I_L2_c
        U_L1_c = I_L1_c * ZL_c  # chute sur la ligne de la section
        U1_c = U0_c - U_L1_c  # tension au bout de la section

        I_L2_c = conj(Sp2_c / U2_c) + I_L3_c
        U_L2_c = I_L2_c * ZL_c  # chute sur la ligne de la section
        U2_c = U1_c - U_L2_c  # tension au bout de la section

        I_L3_c = conj(Sp3_c / U3_c) + I_L4_c
        U_L3_c = I_L3_c * ZL_c  # chute sur la ligne de la section
        U3_c = U2_c - U_L3_c  # tension au bout de la section

        I_L4_c = conj(Sp4_c / U4_c)
        U_L4_c = I_L4_c * ZL_c  # chute sur la ligne de la section
        U4_c = U3_c - U_L4_c  # tension au bout de la section

        gamma = cmath.phase(U4_c)

        convergence_check = (beta_angle - gamma) * 180.0 / pi
        # algo pour convergence
        beta_angle = gamma


        # print(["Loop ", loop , " the delta gamma is: " , convergence_check, "deg" ])

    Urms_iter = [0, 0, 0, 0]
    Urms_iter[0] = abs(U1_c)
    Urms_iter[1] = abs(U2_c)
    Urms_iter[2] = abs(U3_c)
    Urms_iter[3] = abs(U4_c)

    if I_want_plot_local == 1:
        fig2 = plt.figure(2)
        plt.plot([V_grid] + Urms_iter, 'bo-')
        plt.xlabel('Distance')
        plt.ylabel('Volts')
        plt.title("tensions sur la ligne")
        ax = fig2.gca()
        ax.grid(True)

        plt.draw()
        # plt.show(0)

    # print("U2rms_iter= ", Urms_iter )
    # print(" Fin de l iteration et resultat" )
    # print(" " )



    S_trafo_c = U0_c * conj(I_L1_c)
    Q_trafo = imag(S_trafo_c)

    # print("S_trafo_c= ", S_trafo_c )




    # sorties:
    outputs = range(8 + 1)

    outputs[0] = abs(U1_c)  # la tension au noeud 1
    outputs[1] = abs(U2_c)  # la tension au noeud 2
    outputs[2] = abs(U3_c)  # la tension au noeud 3
    outputs[3] = abs(U4_c)  # la tension au noeud 4
    outputs[4] = Q_trafo  # reactif au transfo
    outputs[5] = abs(I_L1_c)  # courant dans la section 1
    outputs[6] = abs(I_L2_c)  # courant dans la section 2
    outputs[7] = abs(I_L3_c)  # courant dans la section 3
    outputs[8] = abs(I_L4_c)  # courant dans la section 4

    return outputs
