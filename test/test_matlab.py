"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

import matlab.engine
import time

import unittest

start_time = 0
elapsed_time = 0


def debug_eval_time(step):
    global start_time
    global elapsed_time
    elapsed_time = time.time()
    print 'step:' + str(step) + ',time:', elapsed_time - start_time
    start_time = elapsed_time


class TestMatlab(unittest.TestCase):
    def test_matlab(self):
        global  start_time
        start_time = time.time()

        mat_engine = matlab.engine.start_matlab()  # first time 14; 5.4
        debug_eval_time('1')

        for i in range(100):  # 2.7
            district, sim_offset, sim_length, sim_step, sim_time = \
                getattr(mat_engine,'simulation_initialize')(nargout=5)  # 0.0027
        debug_eval_time('2')

        for i in range(100):  # 2.5, choix_algo=0, no calculate, without print
                              # 27, choix algo=2, with calculate, without print
            q = getattr(mat_engine, 'simulation_step')(1, [0,0,0], [0.0]*25, nargout=3)  # 0.0025; 0.27

        debug_eval_time('3')

        self.assertEqual(0,q[0])
        self.assertEqual(0,q[1])
        self.assertEqual(0,q[2])