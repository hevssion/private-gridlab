"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridsim.cyberphysical.security import SecurityManager, SecurityGroup, Security
from gridlab.security.current import CurrentSecurity

from gridlab.paramtype import ParamType

import unittest


class TestSecurity(unittest.TestCase):
    def __init__(self):
        super(TestSecurity, self).__init__()

        class TSecurity(Security):
            def __init__(self):
                super(TSecurity, self).__init__()

                self._call_fault = 0
                self._district_name = 'd'

            def priori_security_check(self):
                self._call_fault += 1
                if self._call_fault == 5:
                    return {ParamType._P: 10000, ParamType._Q: 10000}
                return {ParamType._P: 2000, ParamType._Q: 2000}

            def posteriori_security_check(self):
                self._call_fault += 1
                if self._call_fault == 5:
                    return {(ParamType._ISUM, self._district_name, 5): 55}
                return {(ParamType._ISUM, self._district_name, 5): 25}

            def fast_correction(self, write_param):
                print "correction applied"

        self._security = SecurityManager()
        self._security_group = SecurityGroup()
        self._security_rules = CurrentSecurity()

        self._security_group.add_security(TSecurity())
        self._security_group.set_rules(self._security_rules)

    def test_security_priori(self):
        self._security.add_priori_security_group(self._security_group)
        while True:
            self._security.priori_security_check()
            print 'security priori loop'

    def test_security_posteriori(self):
        self._security.add_posteriori_security_group(self._security_group)
        while True:
            self._security.posteriori_security_check()
            print 'security posteriori loop'


if __name__ == '__main__':
    unittest.main()
