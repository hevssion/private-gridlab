"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridlab.gridlabcyberphysicalsystem.svc import SVC

import time
import unittest


class TestSVC(unittest.TestCase):
    def __init__(self):
        super(TestSVC, self).__init__()
        self._svc = SVC("test")

        self._svc.initialize()
        self._svc.dspctrl_connect('153.109.14.63')

    def test_svc_ready(self):
        time.sleep(2)
        cpu1 = self._svc.dspctrl_isready(0)
        cpu2 = self._svc.dspctrl_isready(1)
        cpu3 = self._svc.dspctrl_isready(2)

        self.assertEqual(cpu1, 1)
        self.assertEqual(cpu2, 1)
        self.assertEqual(cpu3, 1)

    def test_svc_read(self):
        time.sleep(2)
        cpu1 = self._svc.dspctrl_read_parameter(0, 4, 6, 0)
        cpu2 = self._svc.dspctrl_read_parameter(1, 4, 6, 0)
        cpu3 = self._svc.dspctrl_read_parameter(2, 4, 6, 0)

        self.assertEqual(cpu1[1], 0)
        self.assertEqual(cpu2[2], 0)
        self.assertEqual(cpu3[3], 0)

    def set_sv_write_enable(self):
        time.sleep(2)
        # enable svc cpu
        cpu1 = self._svc.dspctrl_write_parameter(0, 4, 6, 0, 1)
        cpu2 = self._svc.dspctrl_write_parameter(1, 4, 6, 0, 1)
        cpu3 = self._svc.dspctrl_write_parameter(2, 4, 6, 0, 1)

        self.assertEqual(cpu1, 1)
        self.assertEqual(cpu2, 1)
        self.assertEqual(cpu3, 1)

    def test_svc_read_enable(self):
        time.sleep(2)
        cpu1 = self._svc.dspctrl_read_parameter(0, 4, 6, 0)
        cpu2 = self._svc.dspctrl_read_parameter(1, 4, 6, 0)
        cpu3 = self._svc.dspctrl_read_parameter(2, 4, 6, 0)

        self.assertEqual(cpu1[1], 1)
        self.assertEqual(cpu2[2], 1)
        self.assertEqual(cpu3[3], 1)

    def test_svc_write_setpoint(self):
        time.sleep(2)
        # set voltage level
        cpu1 = self._svc.dspctrl_write_parameter(0, 83, 6, 16, 150.0)  # register 83
        cpu2 = self._svc.dspctrl_write_parameter(1, 83, 6, 16, 230.0)
        cpu3 = self._svc.dspctrl_write_parameter(2, 83, 6, 16, 235.0)

        self.assertEqual(cpu1, 1)
        self.assertEqual(cpu2, 1)
        self.assertEqual(cpu3, 1)

    def test_svc_read_setpoint(self):
        time.sleep(2)
        # read voltage measure
        cpu1 = self._svc.dspctrl_read_parameter(0, 70, 5, 16)
        cpu2 = self._svc.dspctrl_read_parameter(1, 70, 5, 16)
        cpu3 = self._svc.dspctrl_read_parameter(2, 70, 5, 16)

        self.assertAlmostEqual(cpu1[1], 150, 5)
        self.assertAlmostEqual(cpu2[2], 230, 5)
        self.assertAlmostEqual(cpu3[3], 230, 5)


if __name__ == '__main__':
    unittest.main()
