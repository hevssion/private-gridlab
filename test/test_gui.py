"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridlab.actor.guiactor import PQGuiController
from gridlab.paramtype import ParamType

import unittest


class TestGui(unittest.TestCase):
    def test_gui_basic(self):
        district = 'a'

        readparam = [(ParamType._PA,), (ParamType._QA,)]
        writeparam = [(ParamType._P, district, 1), (ParamType._Q, district, 1)]

        guicontroller = PQGuiController(readparam, writeparam)
        guicontroller.start_controller()
        # guicontroller.close_controller()

        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
