"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from pyModbusTCP import constants as const
from pyModbusTCP.utils import crc16

import socket

# create send request message
#
# f_body = struct.pack('B', 0x03) + struct.pack('>HH', 40201, 38 * 2)
#
# slave_ad = struct.pack('B', 15)
# crc = struct.pack('<H', crc16(slave_ad + f_body))
# tx_buffer = slave_ad + f_body + crc
#
# print tx_buffer.encode('hex')
#
#
# # debug connect socket function
#
# def init_socket(host, port):
#     for res in socket.getaddrinfo(host, port,
#                                   socket.AF_UNSPEC, socket.SOCK_STREAM):
#
#         af, sock_type, proto, canon_name, sa = res
#         try:
#             s = socket.socket(af, sock_type, proto)
#         except socket.error:
#             s = None
#             continue
#         try:
#             s.settimeout(30.0)
#             s.connect(sa)
#             # s.setblocking(0)
#         except socket.error:
#             s.close()
#             s = None
#             continue
#         break
#     if s is not None:
#         return s
#     else:
#         return None


# debug send message

# s = init_socket('153.109.14.47', 502)

# s.send(tx_buffer)

# time.sleep(1)
# data = s.recv(1024)
# print len(data)  ##158
# data = data.encode('hex')
# print data
#
#
# res = []
# for i in range(38):
#     d = data[6 + i * 8:6 + (i + 1) * 8]  # get data frame
#     res.append(ModBusOverTcp.measured_value_float_decode(d))
#
# print res

#########################################################

# test new receive modbus function

# def recv_timeout(the_socket, timeout=2):
#     total_data = []
#     len_data = 0
#     begin = time.time()
#     while 1:
#         if len_data == 158:
#             break
#         elif time.time() - begin > timeout * 2:
#             break
#         try:
#             data = the_socket.recv(1024)
#             len_data += len(data)
#             total_data.append(data)
#         except:
#             pass
#     return ''.join(total_data)
#
# s.setblocking(0)
#
# start_time = time.time()
# for i in range(100):
#     s.send(tx_buffer) #handle socket.error: [Errno 10053] An established connection was aborted by the software in your host machine
#     data = recv_timeout(s).encode('hex')
#     if len(data) == 158*2:
#         res = []
#         for i in range(38):
#             d = data[6 + i * 8:6 + (i + 1) * 8]  # get data frame
#             res.append(ModBusOverTcp.measured_value_float_decode(d))
#         print res
#
# print 'end_time:', time.time()-start_time




from pyModbusTCP.client import ModbusClient
from gridlab.modbus.modbus import ModBusOverTcp

from gridlab.gridlabcyberphysicalsystem.district import DEGUC_COM

import struct
import time

import unittest

start_time = 0
elapsed_time = 0


def debug_eval_time(step):
    global start_time
    global elapsed_time
    elapsed_time = time.time()
    diff = elapsed_time - start_time
    print 'step:' + str(step) + ',time:', diff
    start_time = elapsed_time
    return diff


class TestModbus(unittest.TestCase):
    def test_modbus_over_tcp(self):  # last version modbus decode 0.17sec each call

        if DEGUC_COM:
            return

        global start_time

        modbus_over_tcp = ModBusOverTcp('153.109.14.47', 502)
        modbus_over_tcp.open()

        quantity = 38
        start_time = time.time()
        for i in range(100):
            data = modbus_over_tcp.read_holding_registers(15, 40201, quantity * 2).encode('hex')
            # print data
            res = []
            for i in range(quantity):
                d = data[6 + i * 8:6 + (i + 1) * 8]  # get data frame
                if int(d, 16) == 0:
                    res.append(0.0)
                else:
                    # error may occur in measured float value decode function with the d data
                    res.append(ModBusOverTcp.measured_value_float_decode(d))
                    # print res

        self.assertAlmostEqual(0.17 * 100, debug_eval_time(1), delta=1)

    #########################################################

    def test_modbus_tcp(self):

        if DEGUC_COM:
            return

        global start_time

        modbus = ModbusClient('153.109.14.46', 502)  # last version modbus encode write to ABB
        modbus.open()
        print modbus.is_open()
        # modbus.debug(True)

        p = int(struct.pack('>h', 5000 * 10.16 / 22).encode('hex'), 16)
        q = int(struct.pack('>h', (7000 - 2050) / -2.36).encode('hex'), 16)

        # rx_buffer = modbus.write_multiple_registers(1101, [p])
        tx_buffer = modbus.write_multiple_registers(1098, [q, 0, 0, p])

        time.sleep(1)

        rx_buffer = modbus.read_holding_registers(1098, 4)

        self.assertEqual(tx_buffer[0], q)
        self.assertEqual(tx_buffer[3], p)


if __name__ == '__main__':
    unittest.main()
