"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridlab.gridlabcyberphysicalsystem.feeder import Feeder

import unittest


class TestReset(object):
    def __init__(self):
        super(TestReset, self).__init__()

        self._feeder = Feeder('a')
        self._feeder.initialize_district()

    def test_reset(self):
        self._feeder.gridlab_house['gridlab_house_1a'].cyberphysical_module_begin()
        self._feeder.gridlab_house['gridlab_house_2a'].cyberphysical_module_begin()
        self._feeder.gridlab_house['gridlab_house_3a'].cyberphysical_module_begin()

if __name__ == '__main__':
    #unittest.main()

    test = TestReset()
    test.test_reset()
