"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridplot.plot.matplot import MatPlot
from gridlab.paramtype import ParamType

import unittest


class TestMatplot(unittest.TestCase):
    def test_matplot(self):
        matplot = MatPlot('Test Graph')

        matplot.create_plot_data('1Act. P. [W]', [(ParamType._PA, 'a', 1),
                                                 (ParamType._PA, 'a', 2),
                                                 (ParamType._PA, 'a', 3)])
        matplot.create_plot_data('2Act. P. [W]', [(ParamType._PA, 'b', 1),
                                                 (ParamType._PA, 'b', 2),
                                                 (ParamType._PA, 'b', 3)])
        matplot.create_plot_data('3Act. P. [W]', [(ParamType._PA, 'c', 1),
                                                 (ParamType._PA, 'c', 2),
                                                 (ParamType._PA, 'c', 3)])
        matplot.create_plot_data('4Act. P. [W]', [(ParamType._PA, 'd', 1),
                                                 (ParamType._PA, 'd', 2),
                                                 (ParamType._PA, 'd', 3)])

        # matplot.start_ploting()

if __name__ == '__main__':
    unittest.main()