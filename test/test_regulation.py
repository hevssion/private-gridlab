"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridsim.cyberphysical.regulation import RegulationManager
from gridlab.gridlabcyberphysicalsystem.feeder import Feeder

from gridlab.paramtype import ParamType

import unittest

import time

start_time = 0
elapsed_time = 0


def debug_eval_time(step):
    global start_time
    global elapsed_time
    elapsed_time = time.time()
    print 'time ' + str(step) + ':', elapsed_time - start_time
    start_time = elapsed_time


class TestRegulation(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestRegulation, self).__init__(*args,**kwargs)

        self._regulationmanager = RegulationManager(3, 2, True)

        self._feeder = Feeder('a')

        self._feeder.initialize_district()
        self._feeder.register_regulation(self._regulationmanager)

    def test_regulation(self):
        global start_time

        print self._feeder.gridlab_house.keys()

        self._feeder.gridlab_house['gridlab_house_1a'].write_physical = {ParamType._P: 4000, ParamType._Q: 3000}
        self._feeder.gridlab_house['gridlab_house_2a'].write_physical = {ParamType._P: 5000, ParamType._Q: 2000}
        self._feeder.gridlab_house['gridlab_house_3a'].write_physical = {ParamType._P: 6000, ParamType._Q: 1000}

        self._feeder.gridlab_house['gridlab_house_1a'].finish_update()
        time.sleep(1)
        self._feeder.gridlab_house['gridlab_house_2a'].finish_update()
        time.sleep(1)
        self._feeder.gridlab_house['gridlab_house_3a'].finish_update()

        time.sleep(1)

        start_time = time.time()
        self._regulationmanager.init_regulation()
        debug_eval_time(1)
        self._regulationmanager.cycle_regulation()
        debug_eval_time(2)
        self._regulationmanager.end_regulation()
        debug_eval_time(3)

        reg1 = self._feeder.gridlab_house['gridlab_house_1a'].physical_read_params()
        reg2 = self._feeder.gridlab_house['gridlab_house_2a'].physical_read_params()
        reg3 = self._feeder.gridlab_house['gridlab_house_3a'].physical_read_params()

        self.assertAlmostEqual(4000, reg1[16], delta=1000)
        self.assertAlmostEqual(5000, reg2[16], delta=1000)
        self.assertAlmostEqual(6000, reg3[16], delta=1000)
        self.assertAlmostEqual(3000, reg1[20], delta=1000)
        self.assertAlmostEqual(2000, reg2[20], delta=1000)
        self.assertAlmostEqual(1000, reg3[20], delta=1000)


if __name__ == '__main__':
    unittest.main()
